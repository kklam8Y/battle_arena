﻿Shader "Custom/SurfaceReflection" 
{
	Properties
	{

		_MainTex("_Tex", 2D) = "white"{}
		_ReflectTex("_ReflectTex", 2D) = "white"{}
		_ViewPos("_ViewPos", Vector) = (0, 0, 0, 0)
		_LightPos("LightPos", Vector) = (300, 500, 0, 0)
		_Speed("_Speed", float) = 1
		_Value2("_Val_2", float) = 1
		_Value3("_Val_2", float) = 1
		_Amount("_Amount", Range(0,1)) = 0
	}


	SubShader
	{
	//	GrabPass
	//	{
		//	 "_PassOne"
		//}
		CGINCLUDE
	
			//#pragma vertex vertFunc
		//	#pragma fragment fragmentFunc

		#include "UnityCG.cginc"


		struct appdata
		{
			float4 position : POSITION;
			float3 uv_reflect : TEXCOORD0;
			float3 normal : NORMAL;
			float3 uv : TEXCOORD1;
		};

		struct v2f
		{
			float4 position : POSITION;
			float4 uv_reflect : TEXCOORD0;
			float2 uv : TEXCOORD1;
			float4 color : COLOR;
			float3 normal : NORMAL;
			float3 lightDir : COLOR1;
			float3 viewDir : COLOR2;
			float4 ref_param : COLOR3;
		};

		float3 _LightPos;
		float3 _ViewPos;
		float _Speed;
		float _Value2;
		float _Value3;
		float _Amount;

		sampler2D _MainTex;
		sampler2D _ReflectTex;

		v2f vertFunc(appdata IN)
		{
			v2f OUT;

			float phase= _Time.w * _Speed;
			float offset = (IN.position.x + IN.position.z + (IN.position.y * 0.5)) *_Value2;
			IN.position.y = IN.normal.y * sin(phase + offset) * _Value3;


			OUT.position = mul(UNITY_MATRIX_MVP, IN.position);


			OUT.uv = IN.uv ;
			OUT.uv_reflect = ComputeScreenPos(OUT.position + IN.normal.y * sin(phase + offset) * _Value3) ;

			OUT.color = float4(1, 1 ,1, 1.0f);
			OUT.normal = mul(unity_WorldToObject, IN.normal);


			float3 dirLight = normalize((float3)_LightPos - OUT.position);
			float3 viewDir = normalize((float3)_ViewPos - OUT.position);

			OUT.lightDir = dirLight; 
			OUT.viewDir = float4(viewDir.x, viewDir.y, viewDir.z, 0); 

			float r = normalize(ObjSpaceViewDir(OUT.position));
			float d = saturate(dot(r, normalize(OUT.position)));

			OUT.ref_param = float4(d, 0,0,0);

			return OUT;
		}

		fixed4 fragmentFunc(v2f IN) : COLOR
		{

			float2 uv_screen = float2(IN.uv_reflect.x/ IN.uv_reflect.w , -IN.uv_reflect.y/ IN.uv_reflect.w);

			float4 reflection_tex = tex2D(_ReflectTex, uv_screen) ;
			fixed4 water_tex = tex2D(_MainTex, IN.uv);

			float specK = 8;

			float3 diff = saturate(dot(IN.normal,IN.lightDir));

			float3 h = normalize(IN.lightDir + IN.viewDir.xyz);
			float3 nh = dot(h, IN.normal);
			float3 spec = pow(nh, specK) * IN.color.a;//IN.color.a; 
			float3 specColor = float3(0.3f, 0.3f, 0.3f);

			diff = diff*IN.color;
			
			return float4(diff * reflection_tex + diff * water_tex, IN.color.a);
		}

		ENDCG


		Pass
		{
			Tags{"Queue" = "Transparent"}
			Cull Back
			Blend SrcAlpha OneMinusSrcAlpha
			//ZTest off
//			Lighting off
//			Cull off
//			Fog {Mode off}
			//ColorMask RBG
			CGPROGRAM
			#pragma vertex vertFunc
			#pragma fragment fragmentFunc
			ENDCG
		}


	}
}
