﻿Shader "Custom/CCImageEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_EffectTex ("_EffectTex", 2D) = "white" {}
		_Radius("_Radius", float) = 1
		_RadiusOut("_RadiusOut", float) = 1
		_MaxAngle("_MaxAngle", Range(0, 360)) = 1
		_Division("_Division", Range(0, 8)) = 2
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.5
			
			#include "UnityCG.cginc"
			#pragma target 3.5

			struct appdata
			{
				float4 position :POSITION;
				float2 uv : TEXCOORD0;
				float2 uvEffect : TEXCOORD1;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 uvEffect : TEXCOORD1;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.position);
				o.uv = v.uv;
				o.uvEffect = v.uvEffect;
				return o;
			}
			
			sampler2D _MainTex;

			float _Radius;
			float _RadiusOut;
			float _MaxAngle;
			float _Division;

			fixed4 frag (v2f i) : SV_TARGET
			{
				float aspect_ratio = _ScreenParams.y/_ScreenParams.x;

				fixed3 col = tex2D(_MainTex, i.uv);
				fixed3 effectTex = tex2D(_MainTex, float2(i.uvEffect.x * aspect_ratio/_Division, i.uvEffect.y/_Division));
				//fixed3 colHit = tex2D(_Tex, i.uvHit);
				//float atten = _Radius - length(i.vertex.xy - _ScreenParams.xy/2);
				//float c = 1 - (1.0f/0.5f) * atten /_Radius;
				// just invert the colors
				//col = 1 - col;
		
				float4 c = float4(0,0,0,0);

				float offset_height = _ScreenParams.y/2 - 20;

				float d = sqrt(pow((i.pos.x - _ScreenParams.x/2), 2) + pow((i.pos.y -  offset_height), 2));

				float angle = atan2((i.pos.x - _ScreenParams.x/2), (i.pos.y - offset_height));

				if(d > _Radius && d < _RadiusOut && abs(angle) <  0.01745329252f * _MaxAngle/2)
					c = float4(0.5f, 0.5f, 0.5f, 1);
				else
					c = float4(col.rgb, 1.0f);;

		

				//float radiusX = _ScreenParams.x/2 - _OffsetRadiusX;
				//float radiusY = _ScreenParams.y/2 - _OffsetRadiusY;

				//float insideX = pow((i.vertex.x - _ScreenParams.x/2 -12), 2)/pow(radiusX, 2);
				//float insideY = pow((i.vertex.y - _ScreenParams.y/2), 2)/pow(radiusY, 2);

				//float inside = insideX + insideY;

				//inside *= 0.2f + sin(_Amount);

				//float3 c = float4(1 - pow(0.6f *_Amount * inside , 2), 0, 0, 1);

				//if(inside > 1)
				//	col *= c;
				//else 
					//c =	float4(0, 0, 0, 1);;

				return float4(c );
			}
			ENDCG
		}
	}
}
