﻿Shader "Custom/Dissolve"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_CurtainTex("CurtainTex", 2D) = "white" {}
		_GaideTex("GaideTex", 2D) = "white" {}
		_Dissolve("_Dissolve", Range(0,1)) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _CurtainTex;
			sampler2D _GaideTex;
			float _Dissolve;

			fixed4 frag (v2f i) : SV_Target
			{
				i.uv.y = 1 - i.uv.y;
	
				fixed4 col = tex2D(_CurtainTex, i.uv);
				fixed4 gaide = tex2D(_GaideTex, i.uv);
				
				clip(_Dissolve-gaide.rgb);

				return col;
			}
			ENDCG
		}
	}
}
