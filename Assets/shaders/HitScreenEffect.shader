﻿Shader "Custom/HitScreenEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Tex ("TextureHit", 2D) = "white" {}
		_OffsetRadiusX("_RadiusX", float) = 3
		_OffsetRadiusY("_RadiusY", float) = 3
			//_CenterPos("_PosCenter", Vector) = (500, 500) 
		_Amount("_Amount", Range(0, 1)) = 1
		_Active("_Active", int) = 1
	}
	SubShader
	{
		Tags{"Queue" = "Transparent-1"}

		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#pragma target 3.5

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uvHit : TEXCOORD1;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 uvHit : TEXCOORD1;
				
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.uvHit = v.uvHit;
				return o;
			}

			float _OffsetRadiusX;
			float _OffsetRadiusY;
			float _Amount;
			int _Active;
			sampler2D _MainTex;
			sampler2D _Tex;

			fixed4 frag (v2f i) : SV_TARGET
			{
				fixed3 col = tex2D(_MainTex, i.uv);
				fixed3 colHit = tex2D(_Tex, i.uvHit);

				//float atten = _Radius - length(i.vertex.xy - _ScreenParams.xy/2);
				//float c = 1 - (1.0f/0.5f) * atten /_Radius;
				// just invert the colors
				//col = 1 - col;
		

				//float d = sqrt(pow((i.vertex.x - _ScreenParams.x/2), 2) + pow((i.vertex.y -  _ScreenParams.y/2), 2));

				//if(d > _Radius)
					//c = float4(1, 0, 0, 1);
				//else
					//c = float4(0, 0, 0, 0);

				float radiusX = _ScreenParams.x/2 - _OffsetRadiusX;
				float radiusY = _ScreenParams.y/2 - _OffsetRadiusY;

				float insideX = pow((i.pos.x - _ScreenParams.x/2 -12), 2)/pow(radiusX, 2);
				float insideY = pow((i.pos.y - _ScreenParams.y/2), 2)/pow(radiusY, 2);

				float inside = insideX + insideY;

				//inside *= 0.25f + sin(1);

				float3 c =  pow(0.62f *_Amount * inside , 3) *float4( 0.6, 0.0, 0.0f, 1);

				if(inside > 1 && _Active == 1)
					col += c;
				//else 
					//c =	float4(0, 0, 0, 1);;

				return float4(col , 1.0f);
			}

			//float powerForPos(float4 pos, float2 nearVertex)
			//{
				
				//return (1.0/_FogMaxRadius) * atten/_FogRadius;
			//}
			ENDCG
		}
	}
}
