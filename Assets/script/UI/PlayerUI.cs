﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

    public FoVPlayer playerInfoRelay;
    public Image[] ResourcesImage;
    public Image[] abilityImages;
    public  PlayerStatus mPlayerStatus;

    float hMax, hCurrent; //max/current health
    float rMax, rCurrent; //max/current resource

    // Use this for initialization
    void Start () {
        //mPlayerStatus = playerInfoRelay.getLocalPlayer();
        Debug.Log(mPlayerStatus.HeroName);

        hMax = mPlayerStatus.myHero.Health.GetMax();
        rMax = mPlayerStatus.myHero.Resource.GetMax();

        if (abilityImages.Length > mPlayerStatus.UIImages.Length)
            return;

        for (int i = 0; i < abilityImages.Length; i++)
        {
            abilityImages[i].sprite = mPlayerStatus.UIImages[i];;
            abilityImages[i].GetComponent<UIMaterialManager>().PS = mPlayerStatus;
            abilityImages[i].GetComponent<UIMaterialManager>().mAbilityKey = (Ability)i+1;
        }
    }
	
	// Update is called once per frame
	void Update () {
        hCurrent = mPlayerStatus.myHero.Health.current;
        rCurrent = mPlayerStatus.myHero.Resource.current;

        ResourcesImage[0].fillAmount = (float)GetNormalizedValue(hMax, hCurrent);
        ResourcesImage[1].fillAmount = (float)GetNormalizedValue(rMax, rCurrent);

        for (int i = 0; i <  abilityImages.Length; i++)
        {
            abilityImages[i].fillAmount = 1- (float)mPlayerStatus.GetNormAbilityTimer(i);
        }
    }

    private double GetNormalizedValue(float _max, float _current)
    {
        return (1 / _max) * _current;
    }
}
