using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerWorldUIManager : MonoBehaviour {

    public float height = 5f;

	public Image hImage, rImage; //resource health images

	float hMax, hCurrent; //max/current health
	float rMax, rCurrent; //max/current resource

    public Material mat_HealthBar_Friendly, mat_HealthBar_Hostile;
	// Use this for initialization
	void Start ()
    {

        if (GetComponentInParent<PlayerControl>().photonView.isMine)
        {
            hImage.material = mat_HealthBar_Friendly;
            hImage.material.SetColor("_Color", Color.green);
        }
        else
        {
            hImage.material = mat_HealthBar_Hostile;
            hImage.material.SetColor("_Color", Color.red);
        }

        hMax = GetComponentInParent<PlayerStatus> ().myHero.Health.GetMax ();
		rMax = GetComponentInParent<PlayerStatus> ().myHero.Resource.GetMax ();

	}
	
	// Update is called once per frame
	void Update () {

		transform.rotation = Camera.main.transform.rotation;

        Vector3 player_pos = GetComponentInParent<PlayerControl>().transform.position;

		transform.position = new Vector3(player_pos.x, player_pos.y + (GetComponentInParent<Collider>().bounds.size.y/2) + 2, player_pos.z);

		hCurrent = GetComponentInParent<PlayerStatus> ().myHero.Health.current ;
		rCurrent = GetComponentInParent<PlayerStatus> ().myHero.Resource.current ;

		hImage.fillAmount = (float)GetNormalizedValue(hMax, hCurrent);
		rImage.fillAmount = (float)GetNormalizedValue(rMax, rCurrent);
		//Debug.Log ("Health %: " + GetNormalizedHealth () * 100);

	
	}

	private double GetNormalizedValue(float _max, float _current)
	{
		return (1 / _max) * _current;
	}

}
