﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof(Image))]
public class UIMaterialManager : MonoBehaviour {

    public Material mMat;
    Image mImage;
    bool hasFlickered = false;
    public Ability mAbilityKey;
    public PlayerStatus PS;

    public SResource Timer;
	// Use this for initialization
	void Start ()
    {
        mImage = GetComponent<Image>();
        Timer = new SResource(0.25f);
    }
	
	// Update is called once per frame
	void Update () {
        if (mImage.fillAmount == 1 && PS.IsAbilityReady(mAbilityKey))
        {
            mMat.SetInt("_Ready", 1);

            if (hasFlickered == false)
            {
                Timer.Reset();
                hasFlickered = true;
            }
        }
        else
        {
            mMat.SetInt("_Ready", 0);
            hasFlickered = false;
        }

        Timer.CountDown(Time.deltaTime);
        mMat.SetFloat("_Timer", Timer.current);
    }

}
