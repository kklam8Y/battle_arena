using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStatus : Photon.MonoBehaviour{

    public class CRounds
    {
        SResource Counter;
        public bool roundEnd; 

        public CRounds(int _bestOf)
        {
            Counter = new SResource(_bestOf);
            roundEnd = false;
        }

        public SResource getCounter { get { return Counter; } }

        public void RoundOutCome(State currentState)
        {
            if (currentState != State.DEAD)
            {
                PhotonNetwork.player.AddScore(1);
                Counter.current++;
            }
        }

        public bool isVictorious()
        {
            if (Counter.current == Counter.GetMax())
                return true;

            return false;
        }
    }

	CHero Hero; 
 	public SState mState;
    public CRounds mRounds;

    //Hero Cooldowns
    SResource cd_LMB;
	SResource cd_RMB;
	SResource cd_Q;
	SResource cd_SPACE;

    public Material hitMat;

	//public SResource durationDisable;

	public string HeroName;

    public Renderer [] mStencil;

    public Canvas mStencilHP;

    public Sprite[] UIImages;

    public bool reset = false;

    void Awake()
	{	
		//Debug.Log (Hero.Health.GetMax ());
		if (HeroName == "ZugZug")
			Hero = new CHeroZugZug ();
		else if (HeroName == "Witch")
			Hero = new CHeroJieTu ();
        else if (HeroName == "Skeleton")
            Hero = new CHeroSkeleton();
        //else if (HeroName == "Dummy")
        //Hero = new CDummyHero ();

        mState = new SState();

		mState.SetState (State.IDLE);

        mRounds = new CRounds(3);

        //		for (int i = 0; i < 2; i++) 
        //		{
        //			GameObject temp = Resources.Load (myHero.GetAbility (i).getName) as GameObject;
        //
        //			AbilityPrefabs [i] = temp;
        //			Debug.Log(AbilityPrefabs[i].name);
        //		}

	}

	// Use this for initialization
	void Start () 
	{
		///NetworkManager.NM.player = 
		//Debug.Log(Hero.GetAbility(Ability.LMB).getDamage);
		//CAbilityProjectile mProj = Hero.GetAbility (Ability.RMB) as CAbilityProjectile;

		//Debug.Log(mProj.Distance);

		cd_LMB = myHero.GetAbility (Ability.LMB).getCD;
		cd_RMB = myHero.GetAbility (Ability.RMB).getCD;
		cd_Q = myHero.GetAbility (Ability.Q).getCD;
		cd_SPACE = myHero.GetAbility (Ability.SPACE).getCD;

        mStencil = GetComponentsInChildren<Renderer>();
        mStencilHP = GetComponentInChildren<Canvas>();


        if (photonView.isMine)
        {
            foreach (Renderer r in mStencil)
            {
                if (r.gameObject.tag == "StencilPlayer")
                    r.materials[1].shader = NetworkManager.Single.Mask;
            }

            mStencilHP.enabled = true;

        }

        //Debug.Log ("SPACE CD: " + cd_SPACE.GetMax ());
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown (KeyCode.T))
        	mState.SetState(State.DEAD);
        //else if (Input.GetKeyDown (KeyCode.N))
        //	mState.SetState(State.IDLE);

        //Debug.Log ("current Resource: " + Hero.Resource.current);
        //Debug.Log ("State: " + mState.GetCurrentState);
        //Debug.Log (cd_RMB.current);
        //Debug.Log(cd_LMB.current);
        


        cd_LMB.CountDown(Time.deltaTime);
        cd_RMB.CountDown(Time.deltaTime);
        cd_Q.CountDown(Time.deltaTime);
        cd_SPACE.CountDown(Time.deltaTime);
   

        if (Hero.Health.current <= 0)
			mState.SetState(State.DEAD);

        Hero.Health.current = Mathf.Clamp ((float)myHero.Health.current, 0, (float)myHero.Health.GetMax());
		Hero.Resource.current = Mathf.Clamp ((float)myHero.Resource.current, 0, (float)myHero.Resource.GetMax());

        if (photonView.isMine)
        {
            if (Hero.Health.current / Hero.Health.GetMax() * 100 < 35)
                PostProcessing.Single._matHit.SetInt("_Active", 1);
            else
                PostProcessing.Single._matHit.SetInt("_Active", 0);
        }
    }

	public CHero myHero{get { return Hero;}}

	[PunRPC] public void TakeDamage(int damage)
	{
		Hero.TakeDamage (damage);
	}

	[PunRPC] public void TakeHealing(int healing)
	{
		Hero.TakeHealing (healing);
	}

	[PunRPC] public void ResourceCost (int _cost)
	{
		Hero.Resource.current -= _cost;
	}

	[PunRPC] public void ResourceGain(int _gain)
	{
		Hero.Resource.current += _gain;
	}

	public bool IsAbilityReady(Ability _ability)
	{
		if (_ability == Ability.LMB)
			return cd_LMB.isReady()  && myHero.Resource.current >= myHero.GetAbility(Ability.LMB).getProperty.getPResource.Cost;
        if (_ability == Ability.RMB)
            return cd_RMB.isReady() && myHero.Resource.current >= myHero.GetAbility(Ability.RMB).getProperty.getPResource.Cost; ;
		if (_ability == Ability.Q)
			return cd_Q.isReady() && myHero.Resource.current >= myHero.GetAbility(Ability.Q).getProperty.getPResource.Cost; ;
		if (_ability == Ability.SPACE)
			return cd_SPACE.isReady() && myHero.Resource.current >= myHero.GetAbility(Ability.SPACE).getProperty.getPResource.Cost; ;

		return false;
	}

	public void ResetAbilityCD(Ability _ability)
	{

        if (_ability == Ability.LMB)
            cd_LMB.Reset();
		if (_ability == Ability.RMB)
			cd_RMB.Reset ();
		if (_ability == Ability.Q)
			cd_Q.Reset ();
		if (_ability == Ability.SPACE)
			cd_SPACE.Reset ();
	}

    public double GetNormAbilityTimer(int _abilityNo)
    {
        if (_abilityNo == 0)
            return GetNormalizedValue(cd_LMB.GetMax(), cd_LMB.current);
        if (_abilityNo == 1)
            return GetNormalizedValue(cd_RMB.GetMax(), cd_RMB.current);
        if (_abilityNo == 2)
            return GetNormalizedValue(cd_Q.GetMax(), cd_Q.current);
        if (_abilityNo == 3)
            return GetNormalizedValue(cd_SPACE.GetMax(), cd_SPACE.current);

        return 0;
    }

	void OnGUI()
	{
	//	int size = 7;

	//	string[] str = new string[size];

	//	str [0] = myHero.Health.current.ToString();
		//str [1] = mState.GetCurrentState.ToString();
	//	str [2] = GetComponent<PlayerControl>().mAbilityUsed.GetCurrent.ToString();
	//	str [3] = cd_LMB.current.ToString();
	//	str [4] = cd_RMB.current.ToString();
	//	str [5] = cd_Q.current.ToString();
	//	str [6] = cd_SPACE.current.ToString();

	//	GUI.Label (new Rect (0.9f * Screen.width, 0.1f * Screen.height, 100, 30), PhotonNetwork.GetPing().ToString());



		if (photonView.isMine) 
		{
            GUI.Label(new Rect(0.5f * Screen.width - 0.1f * Screen.width, 0.1f * Screen.height, 100, 30), mRounds.getCounter.current.ToString());
        }
        else 
            GUI.Label(new Rect(0.5f * Screen.width + 0.1f * Screen.width, 0.1f * Screen.height, 100, 30), mRounds.getCounter.current.ToString());

        //		int i = 0;

        //		for (i = 0; i < size; i++)
        //		{
        //			GUI.Label (new Rect (0.1f * Screen.width, 0.4f * Screen.height - 20 * i, 100, 30), str [i]);
        //		}
        //		GUI.Label(new Rect(0.1f * Screen.width, 0.2f * Screen.height - (i * 20) , 100, 30), PhotonNetwork.GetPing().ToString());
        //	} else
        //	{
        //		for (int i = 0; i < size; i++) {
        //			GUI.Label (new Rect (0.7f * Screen.width, 0.2f * Screen.height - 20 * i, 100, 30), str [i]);
        //		}
        //	}
    }
   
	public void SetPlayerHero(CHero _h)
	{
		Hero = _h;
	}

    private double GetNormalizedValue(float _max, float _current)
    {
        return (1 / _max) * _current;
    }

    [PunRPC]public void Reset()
    {

        mState.SetState(State.IDLE);
        myHero.Health.current = myHero.Health.GetMax();
        myHero.Resource.current = 25;
    }
 
}
