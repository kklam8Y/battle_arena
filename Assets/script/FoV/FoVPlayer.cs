﻿using UnityEngine;
using System.Collections;
//using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;

public class FoVPlayer : MonoBehaviour {

	public LayerMask obstMask;

	public PlayerStatus[] Players;

    public PlayerStatus LocalPlayer, enemyPlayer;
    public bool canTrack = false;

	Vector3 myPos, targetPos;
	[SerializeField] Vector3 offset = new Vector3(0, 0.1f, 0);

	uint i = 0;
	int turns, angle = 0;
	int total_angle = 0;
	float dstToTarget = 0.0f;
	Vector3[] dirToTarget;

	public int meshResolution;
	public MeshFilter viewMeshFilter;
	public MeshCollider viewCollider;
	Mesh viewMesh;

    IEnumerator couroutine;

	public float maskCutwayDist = 0.1f;

	int vertexCount;
	Vector3[] vertecies;
	int[] triangles;

	RaycastHit hit, hitPlayer;

	[SerializeField] Collider[] allVisableObjects;

    Vector3 dirPlayer, startPosPlayer;

    public LayerMask ColMask;

    // Use this for initialization
    void Start ()
	{
		viewMesh = new Mesh ();
		viewMesh.name = "view mesh";
		viewMeshFilter.mesh = viewMesh;
	    //viewCollider.sharedMesh = viewMesh;

		angle = 5;
		turns = 360 / angle;

		dirToTarget = new Vector3[turns];

		FindAllVisableTagets ();

		vertexCount = turns + 1;
		vertecies = new Vector3[vertexCount ];
		triangles = new int [(vertexCount ) * 3];

		for(int p = 0; p < vertexCount - 1; p++)
		{
			vertecies[p + 1] = transform.InverseTransformPoint(dirToTarget[p] + transform.position);
			vertecies [p].y = 1;
			if(p < vertexCount - 2)
			{
				triangles[p * 3] = 0;
				triangles[p * 3 + 1] = p + 1;
				triangles[p * 3 + 2] = p + 2;
			}
		}

		triangles[triangles.Length - 1] = vertecies.Length - 1;
		triangles[triangles.Length - 2] = 0;
		triangles[triangles.Length - 3] = 1;

		viewMesh.Clear();
		viewMesh.vertices = vertecies;
		viewMesh.triangles = triangles;
		viewMesh.RecalculateNormals();

		FindAllVisableTagets ();

        //Players = FindObjectsOfType<PlayerStatus> ();

        //foreach (PlayerStatus player in Players) 
        //{

        //          if (player.photonView.isMine)
        //          {
        //              LocalPlayer = player;
        //          }
        //          else
        //              enemyPlayer = player;

        //      }

        LocalPlayer = GetComponentInParent<PlayerStatus>();
    }

	IEnumerator FindVisableObjectsWithDelay(float delay_t)
	{
		while (true) 
		{
			yield return new WaitForSeconds (delay_t);
            TraceOtherPlayers();
		}
	}

	void FindAllVisableTagets()
	{
		total_angle = 0;

		for (i = 0; i < turns ; i++)
		{
			
			myPos = transform.position + offset;
			//targetPos = allVisableObjects [i].transform.position + offset;

			//dirToTarget.x = myPos.x + Mathf.Sin(angle * Mathf.Deg2Rad) * 2;
			//dirToTarget.y = 3;
			//dirToTarget.z = myPos.z + Mathf.Cos(angle * Mathf.Deg2Rad) * 2;

			//dirToTarget = (targetPos - myPos).normalized;
			//dstToTarget = Vector3.Distance (myPos, targetPos);

			//Debug.DrawLine (myPos, myPos + dirToTarget[i], Color.red);

			dirToTarget [i] = (Quaternion.Euler (0, total_angle, 0) * Vector3.forward).normalized * 42;
			//Debug.DrawLine (myPos, myPos + dirToTarget[i], Color.red);



			if (Physics.Raycast (myPos, dirToTarget[i], out hit, 42, obstMask))
			{
		
					//if(hit.collider.GetComponent<Collider>() == allVisableObjects[i].GetComponent<Collider>())
					//Debug.DrawLine (myPos, hit.point, Color.green);
				if (hit.collider.tag == "env") 
				{
					dirToTarget [i] = (Quaternion.Euler (0, total_angle, 0) * Vector3.forward).normalized * hit.distance;
				}

			}
			total_angle += angle;

		}
	}

    void TraceOtherPlayers()
    {
        foreach (Renderer r in enemyPlayer.mStencil)
        {
            if (r.gameObject.tag == "StencilPlayer")
                r.materials[1].shader = NetworkManager.Single.Object;
        }


        enemyPlayer.mStencilHP.enabled = false;

        startPosPlayer = LocalPlayer.transform.position + Vector3.up * 1;
            dirPlayer = (enemyPlayer.transform.position - LocalPlayer.transform.position).normalized;

            Debug.DrawRay(startPosPlayer, dirPlayer, Color.red);

            //Debug.Log("casting ray");
            if (Physics.Raycast(startPosPlayer, dirPlayer, out hitPlayer, 30, ColMask, QueryTriggerInteraction.Ignore))
            {
                if (hitPlayer.transform.tag == "Player")
                {
                    Debug.DrawLine(startPosPlayer, enemyPlayer.transform.position + Vector3.up);

                foreach (Renderer r in enemyPlayer.mStencil)
                {
                    if (r.gameObject.tag == "StencilPlayer")
                        r.materials[1].shader = NetworkManager.Single.Mask;
                }
                enemyPlayer.mStencilHP.enabled = true; 

            }
            }
    }


	void DrawFieldOfView()
	{
		
		//vertecies[0] = Vector3.zero + offset;


		for(int p = 0; p < vertexCount - 1; p++)
		{

			vertecies[p + 1] = transform.InverseTransformPoint(dirToTarget[p] + transform.position);
			vertecies [p].y = 0.1f;
		}

		vertecies [vertecies.Length - 1].y = 0.1f;
		viewMesh.vertices = vertecies;
	}

	// Update is called once per frame
	void Update () 
	{

        if(canTrack && enemyPlayer != null)
            TraceOtherPlayers();

        transform.position = new Vector3 (NetworkManager.Single.localPlayer.transform.position.x, 0.0f, NetworkManager.Single.localPlayer.transform.position.z);

		//if (distance > 1f) {
			FindAllVisableTagets ();
			DrawFieldOfView ();
		//}
//		if (GetComponent<PlayerControl> ().photonView.isMine) {
//			Vector3 screenPos = Camera.main.WorldToScreenPoint (transform.position);
//			Ray toPlayerPos = Camera.main.ScreenPointToRay (screenPos);
//
//			RaycastHit hit;
//
//			if (Physics.Raycast (toPlayerPos, out hit, 100, 1 << 9)) {
//				FOWPlaneTransform.GetComponent<Renderer> ().sharedMaterial.SetVector ("_PlayerPos", hit.point);
//			}
//		}
	}

	void LateUpdate()
	{

	}


}
