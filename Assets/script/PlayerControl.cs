using UnityEngine;
using System.Collections;
using System;

public class PlayerControl : Photon.MonoBehaviour
{
	public float rotation_smoothness = 0;

    float last_sync_time = 0f;
    float sync_delay = 0f;
    float sync_time = 0f;
	Vector3 sync_start_pos = Vector3.zero;
	Vector3 sync_end_pos = Vector3.zero;
	Quaternion sync_start_rot = Quaternion.identity;
	Quaternion sync_end_rot = Quaternion.identity;
	public State sync_state = State.IDLE; 
	public Ability sync_ability_used = Ability.NONE; 
	//string sync_ability_input = KeyCode.None.ToString();
	[SerializeField] float m_SyncSpeed = 0;
	[SerializeField] float m_SyncRot = 0;
	int sync_state_int = 0;
	int sync_ability_used_int = 0;
	//CAbility mAbility;
	Vector3 direction_to_target = Vector3.zero;
    Quaternion look_Rot, m_RotLate = Quaternion.identity;
    Vector3 desiredPosSync;

    float HAxis, VAxis = 0;

	RaycastHit hit;

    public float height = 10.0f;
    public float length = 10.0f;

    Rigidbody rig;

	int scale_factor = 20;
	float speed; 

	PlayerStatus PS;
	PlayerAnimatorControl PAC;

	PhotonTransformView m_TransformView;

	public GameObject [] AbilityPrefabs;

    float deadTime = 0;

    bool reset, sync_reset;

	//GameObject mProj;sd
	[SerializeField] string current_state;

    public SResource deathTimer;

	public SAbility mAbilityUsed = new SAbility ();

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //stream.SendNext(GetComponent<Rigidbody>().position);
            stream.SendNext(GetComponent<Rigidbody>().rotation);
			stream.SendNext ((int)PS.mState.GetCurrentState);
			stream.SendNext((int)mAbilityUsed.GetCurrent);
            //stream.SendNext(desiredPosSync);
            stream.SendNext((bool)reset);
        }
        	else
        {
			
            //Vector3 sync_pos = (Vector3)stream.ReceiveNext();
           	Quaternion sync_rot = (Quaternion)stream.ReceiveNext();
            sync_time = 0.0f;
           	sync_delay = Time.time - last_sync_time;
            last_sync_time = Time.time;
           // sync_end_pos = sync_pos ;
			//sync_start_pos = transform.position;
            sync_end_rot = sync_rot;
			sync_start_rot = transform.rotation;
			sync_state_int = (int)stream.ReceiveNext ();
			sync_ability_used_int = (int)stream.ReceiveNext ();
			sync_state = (State)Enum.ToObject(typeof(State), sync_state_int);
			sync_ability_used = (Ability)Enum.ToObject (typeof(Ability), sync_ability_used_int);
            //desiredPosSync = (Vector3)stream.ReceiveNext();
            sync_reset = (bool)stream.ReceiveNext();
        }
    }

    // Use this for initializatino
    void Start()
    {
        rig = GetComponent<Rigidbody>();
		PS = GetComponent<PlayerStatus> ();
		PAC = GetComponent<PlayerAnimatorControl> ();
		speed = (float)PS.myHero.MoveSpeed.GetOriginal () * Time.deltaTime;
		speed /= scale_factor;

		m_TransformView = GetComponent<PhotonTransformView> ();

        deathTimer = new SResource(3);
        deathTimer.current = deathTimer.GetMax();

    }



    // Update is called once per frame
    void Update()
    {

        if(PS.mState.GetCurrentState != State.DEAD && deathTimer.current < deathTimer.GetMax() )
        {
            deathTimer.Reset();
        }


        if (PS.mState.GetCurrentState == State.DEAD)
        {
            deathTimer.CountDown(Time.deltaTime);

            if (deathTimer.current <= 0)
            {
                PlayerStatus[] allPS = FindObjectOfType<FoVPlayer>().Players;

                foreach (PlayerStatus ps in allPS)
                {
                    NetworkManager.Single.RespawnPlayer();

                    if (ps != PS)
                    {
                        //ps from the array of allPlayers ; PS is of this object.
                        ps.mRounds.RoundOutCome(ps.mState.GetCurrentState);
                          deathTimer.Reset();
                    }
                }

              

                return; // no need to perform any other action while dead
            }
        }


		if (photonView.isMine && PS.mState.GetCurrentState != State.DEAD)
        {
			Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, new Vector3 (transform.position.x, transform.position.y + height, transform.position.z - length), speed);


			if (PS.mState.GetCurrentState != State.STUNNED)
			{
				UseAbility ();
			
				RotateToMouseCurser ();

				if (PS.myHero.GetAbility(mAbilityUsed.GetCurrent).getProperty.canMove && PS.mState.GetCurrentState != State.ROOTED)
				{
					MoveInput ();
				}
			} 
				
			m_SyncRot = (transform.eulerAngles.y - look_Rot.eulerAngles.y)/Time.deltaTime;

			m_SyncRot = (transform.rotation.eulerAngles.y - m_RotLate.eulerAngles.y)/Time.deltaTime;

			m_TransformView.SetSynchronizedValues(transform.position.normalized * m_SyncSpeed, m_SyncRot);


          
        } 
		else 
		{
			//SyncMoveInput ();
			PS.mState.SetState(sync_state);
			mAbilityUsed.SetAbility(sync_ability_used);
            //PS.ResetAbilityCD (sync_ability_used);
            //Debug.Log (mAbilityUsed.GetCurrent);
        }

    }


	void LateUpdate()
	{
		m_RotLate = transform.rotation;	
	}

    private void RotateToMouseCurser()
	{
		CAbility mAbility = PS.myHero.GetAbility (mAbilityUsed.GetCurrent);

        if (mAbility.getType == AbilityType.DASH)
        {
            rotation_smoothness = 0;
        }

        rotation_smoothness = 5;
        //}
        //if(mAbilityUsed.GetCurrent.
        if (mAbilityUsed.GetCurrent != Ability.NONE)
			rotation_smoothness = 2f;

        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);

		if (Physics.Raycast(r, out hit, 100, 5<<6))
        {
			direction_to_target = transform.position - (hit.point + transform.position.y * Vector3.up);
			look_Rot = Quaternion.LookRotation (-direction_to_target);
			transform.rotation = Quaternion.Slerp(transform.rotation, look_Rot, rotation_smoothness * Time.deltaTime);
            
        }
    }

    private void MoveInput()
    {
        HAxis = Input.GetAxis("Horizontal");
        VAxis = Input.GetAxis("Vertical");

		float speed = (float)(PS.myHero.MoveSpeed.current * Time.deltaTime) / scale_factor;

		if (mAbilityUsed.GetCurrent != Ability.NONE)
			speed *= 0.5f;

		rig.transform.Translate(Vector3.right * HAxis * speed, Space.World);
		rig.transform.Translate(Vector3.forward * VAxis * speed, Space.World);

		m_SyncSpeed = speed;

		if (VAxis == 0 && HAxis == 0) 
		{
			PS.mState.SetState (State.IDLE);
			m_SyncSpeed = 0;
		} 
		else 
			PS.mState.SetState (State.MOVING);
    }

    private void SyncMoveInput()
    {
        sync_time += Time.deltaTime;

        Vector3 new_pos;
        Quaternion new_rot;

		sync_end_pos = sync_start_pos + (sync_start_pos.normalized * m_SyncSpeed);

		//new_pos = Vector3.Lerp(sync_start_pos, sync_end_pos, sync_time/  sync_delay);
		new_rot = Quaternion.Slerp(sync_start_rot, sync_end_rot, sync_time  /  sync_delay);

        //rig.position = new_pos;
        rig.rotation = (Quaternion)new_rot;
    }

    private void UseAbility()
    {

		if (mAbilityUsed.GetCurrent == Ability.NONE) 
		{
			//if (PS.myHero.Resource.current >= PS.myHero.GetAbility (mAbilityUsed.GetCurrent).getProperty.Cost)
			//{
	
			if (Input.GetMouseButton (0) && PS.IsAbilityReady (Ability.LMB)) 
			{
				mAbilityUsed.SetAbility (Ability.LMB);
			}

			if (Input.GetMouseButton (1) && PS.IsAbilityReady (Ability.RMB))// && PS.myHero.Resource.current >= PS.myHero.GetAbility (Ability.RMB).getProperty.getPResource.Cost)
			{
				mAbilityUsed.SetAbility (Ability.RMB);
			}

			if (Input.GetKey (KeyCode.Q) && PS.IsAbilityReady (Ability.Q))// && PS.myHero.Resource.current >= PS.myHero.GetAbility (Ability.Q).getProperty.getPResource.Cost) 
			{
				mAbilityUsed.SetAbility (Ability.Q);
			}
			if (Input.GetKey (KeyCode.Space) && PS.IsAbilityReady (Ability.SPACE))// && PS.myHero.Resource.current >= PS.myHero.GetAbility (Ability.SPACE).getProperty.getPResource.Cost) 
			{
				mAbilityUsed.SetAbility (Ability.SPACE);
			}
			//}
		}
	
    }

	public void CreateProjectile(Ability _ability)
	{
		GameObject mProj = AbilityPrefabs [(int)_ability - 1];


        mProj.GetComponent<AbilityProjectileBehaviour> ().instigator = gameObject;
		mProj.GetComponent<AbilityProjectileBehaviour> ().mAbilityKey = _ability;

		Instantiate (mProj, transform.position, Quaternion.identity);
	}

	public void CreateGeneric(Ability _ability)
	{
		GameObject mGen = AbilityPrefabs [(int)_ability - 1];

		//Debug.Log (PS.myHero.GetAbility (_ability).getName);

        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(r, out hit, 100, 5 << 6))
        {
            if (hit.collider.tag == "ground")
            {
               PS.myHero.GetAbility((int)_ability - 1).clickPos = hit.point;
            }
        }

       // Debug.Log(PS.myHero.GetAbility((int)_ability - 1).clickPos);

        mGen.GetComponent<AbilityGenericBehaviour> ().instigator = gameObject;
		mGen.GetComponent<AbilityGenericBehaviour> ().mAbilityKey = _ability;

		Instantiate(mGen, transform.position, transform.rotation);
	}

	public void CreateDash(Ability _ability)
	{
		GameObject mDash = AbilityPrefabs [(int)_ability - 1];

		Debug.Log (PS.myHero.GetAbility (_ability).getName);

		mDash.GetComponent<AbilityDashBehaviour> ().instigator = gameObject;
		mDash.GetComponent<AbilityDashBehaviour> ().mAbilityKey = _ability;

		Instantiate (mDash, transform.position, Quaternion.identity);
	}

	public void CreateTeleport(Ability _ability)
	{
		GameObject mGen = AbilityPrefabs [(int)_ability - 1];

		Debug.Log (PS.myHero.GetAbility (_ability).getName);

		mGen.GetComponent<AbilityTeleportBehaviour> ().instigator = gameObject;
		mGen.GetComponent<AbilityTeleportBehaviour> ().mAbilityKey = _ability;

		Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (r, out hit, 100, 5 << 6)) 
		{
			if (hit.collider.tag == "ground") 
			{
				mGen.GetComponent<AbilityTeleportBehaviour> ().desiredPos = hit.point;
                Instantiate(mGen, transform.position, transform.rotation);
            }
		}

    }

    public void CreateAOE(Ability _ability, Vector3 _pos)
    {
        GameObject mGen = AbilityPrefabs[(int)_ability - 1];

        Debug.Log(PS.myHero.GetAbility(_ability).getName);

        mGen.GetComponent<AbilityAOEBehaviour>().instigator = gameObject;
        mGen.GetComponent<AbilityAOEBehaviour>().mAbilityKey = _ability;

        CAbilityAOE mAOE = PS.myHero.GetAbility(_ability) as CAbilityAOE;

        if (mAOE.isAttached)
            Instantiate(mGen, transform.position, transform.rotation);
        else
            Instantiate(mGen, _pos, transform.rotation);

    }


    [PunRPC] public void CreateAbilityByType(Ability _ability_button, Vector3 _pos)
	{
		switch (PS.myHero.GetAbility (_ability_button).getType)
		{
		case AbilityType.GENERIC:
			CreateGeneric (_ability_button);
			//photonView.RPC("CreateGeneric", PhotonTargets.Others, _ability_button);
			break;
		case AbilityType.PROJECTILE:
			CreateProjectile (_ability_button);
			//photonView.RPC("CreateProjectile", PhotonTargets.Others, _ability_button);
			break;
		case AbilityType.TELEPORT:
			CreateTeleport (_ability_button);
			//photonView.RPC("CreateProjectile", PhotonTargets.Others, _ability_button);
			break;
		case AbilityType.DASH:
			CreateDash (_ability_button);
			//photonView.RPC("CreateProjectile", PhotonTargets.Others, _ability_button);
			break;
        case AbilityType.AOE:
            CreateAOE(_ability_button, _pos);
            break;
                //
                //return;
        }
    }
	
    [PunRPC] void GetDesiredPos(Vector3 pos)
    {
        desiredPosSync = pos; 
    }



}

