﻿using UnityEngine;
using System.Collections;

public class CCEffectScreen : MonoBehaviour {


	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
        Graphics.Blit(source, destination, PostProcessing.Single._matCC);
    }
}
