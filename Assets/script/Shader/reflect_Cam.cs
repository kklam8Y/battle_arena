﻿using UnityEngine;
using System.Collections;

public class reflect_Cam : MonoBehaviour {

	public Camera cam;

	public Material _mat;
	Rect r;

	public Texture2D tex;
	void Awake()
	{
		tex = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
		//QualitySettings.masterTextureLimit = 2;
		r = new Rect(0, 0, Screen.width, Screen.height);

	}

	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		//GetComponent<Camera> ().targetTexture = source;
		//Graphics.Blit (source, destination, _mat);
		//_mat.SetTexture("_ReflectTex", source);
	}

	void OnPostRender()
	{
		//tex = new Texture2D (Screen.width, Screen.height);
		tex.ReadPixels(r,0,0);

		tex.Apply ();
		_mat.SetTexture("_ReflectTex", tex);

		////QualitySettings.masterTextureLimit = 0;
	}

}
