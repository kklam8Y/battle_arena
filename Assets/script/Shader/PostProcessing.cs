﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Camera))]
public class PostProcessing : MonoBehaviour {

    private static PostProcessing pp;

    public static PostProcessing Single
    { get{ return pp; } }

	public Material _matHit, _matEntry, _matCC;

    float amount;
	bool asc = true;
	public float speed = 1.2f;

    public Texture tex;
    bool playEntry = false;

    SResource Timer;

    // Use this for initialization
    void Start ()
    {
        pp = GetComponent<PostProcessing>();

        Timer = new SResource(1);
        Timer.current = Timer.GetMax();

        _matHit.SetFloat("_Active", 0);
        _matEntry.SetTexture("_MainTex", tex);
        _matCC.SetFloat("_MaxAngle", 0);
    }

    void Update()
    {

        if (asc)
            amount += Time.deltaTime * speed;

        else if (!asc)
            amount -= Time.deltaTime * speed;

        if (amount > 1)
            asc = false;
        else if (amount < 0.0f)
            asc = true;

        if (playEntry)
        {
            Timer.CountDown(Time.deltaTime);

            _matEntry.SetFloat("_Dissolve", Timer.current);

            if (Timer.isReady())
            {
                playEntry = false;
            }
        }

    }

	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
        _matEntry.SetFloat("_Dissolve", Timer.current);
        _matHit.SetFloat("_Amount", Mathf.Sin(amount));

        Graphics.Blit(source, destination, _matHit);

        if(Timer.current > 0)
            Graphics.Blit(source, destination, _matEntry);

       

    }

    public void StartEntry()
    {
        playEntry = true;
        Timer.Reset();
    }
}
