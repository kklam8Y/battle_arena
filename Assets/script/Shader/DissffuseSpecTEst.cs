﻿using UnityEngine;
using System.Collections;

public class DissffuseSpecTEst : MonoBehaviour {

	public Camera c, reflection; 
	public Light LightSource;
	public Material _mat;

	// Use this for initialization
	void Awake () {
		//LightSource.transform.position;
		Vector3 cam_pos = c.transform.position;
		Quaternion cam_rot = c.transform.rotation;
		//reflection.transform.rotation = new Quaternion (cam_rot.x, cam_rot.y, 0.5f * cam_rot.z, cam_rot.w);
		reflection.transform.rotation = cam_rot;
		reflection.transform.Rotate(-90,0,0, Space.Self);
		reflection.transform.position = new Vector3 (cam_pos.x, -cam_pos.y, cam_pos.z);

	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Renderer>().sharedMaterial.SetVector("_LightPos", LightSource.transform.position);
		GetComponent<Renderer>().sharedMaterial.SetVector("_ViewPos", c.transform.position);

		Vector3 cam_pos = c.transform.position;
		Quaternion cam_rot = c.transform.rotation;

		reflection.transform.rotation = new Quaternion (0.5f *cam_rot.x, cam_rot.y, cam_rot.z, cam_rot.w);
		reflection.transform.rotation = cam_rot;
		reflection.transform.Rotate(-95,0,0, Space.Self);
		reflection.transform.position = new Vector3 (cam_pos.x, -cam_pos.y, cam_pos.z);
		GetComponent<Renderer>().sharedMaterial.SetFloat("_Amount", reflection.transform.localEulerAngles.y/360);

	}
}
