﻿using UnityEngine;
using System.Collections;


public class NetworkManager : Photon.MonoBehaviour
{

    private const string room_name = "Kappa";
    private TypedLobby lobbyName = new TypedLobby("New Lobby", LobbyType.Default);
	private RoomInfo[] room_list;

	string selectedRoom;

	public GameObject[] HeroPrefabs;
	public GameObject FoV, UI;

	public GameObject localPlayer;
    GameObject localFoV, player, localUI;

    [SerializeField]public PlayerStatus[] AllPlayers;

	private static NetworkManager nm;

    public static NetworkManager Single
    {
        get
        {
            return nm;
        }
    }

	public int n = 0;

    [SerializeField] int playerNum = 0;

    public Canvas heroSelect;

    public PostProcessing PP;

    bool foundAllPlayers = false;

    SResource timer;

    RoomOptions roomOptions;

    public Shader Mask, Object;

    // Use this for initialization
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("v4.2");

        roomOptions = new RoomOptions();

        nm = GetComponent<NetworkManager>();

        timer = new SResource(1.5f);
        timer.current = timer.GetMax();
    }

    // Update is called once per frame
    void Update()
    {
        Application.runInBackground = true;

		if(Input.GetKey(KeyCode.Alpha1))
			n = 0;
		if(Input.GetKey(KeyCode.Alpha2))
			n = 1;
        if (Input.GetKey(KeyCode.Alpha3))
            n = 2;

        if (Input.GetKeyDown (KeyCode.B)) 
		{
            RespawnPlayer();
           // photonView.RPC("RespawnPlayer", PhotonTargets.AllBufferedViaServer);
        }
        if (PhotonNetwork.playerList.Length > 1)
            timer.CountDown(Time.deltaTime);
        else
        {
            foundAllPlayers = false;
            timer.Reset();
        }

        if (timer.current <= 0 && !foundAllPlayers)
        {
            AllPlayers = FindObjectsOfType<PlayerStatus>();
            foundAllPlayers = true;

            int b = 0;

            for (int t = 0; t < 2; t++)
            {
                if (AllPlayers[t].photonView.isMine)
                {
                    AllPlayers[t].GetComponentInChildren<FoVPlayer>().Players = AllPlayers;
                    AllPlayers[t].GetComponentInChildren<FoVPlayer>().canTrack = true;
                    b = t;
                }
            }

            if (b == 0)
                AllPlayers[b].GetComponentInChildren<FoVPlayer>().enemyPlayer = AllPlayers[1];
            else if (b == 1)
                AllPlayers[b].GetComponentInChildren<FoVPlayer>().enemyPlayer = AllPlayers[0];
        }

        if (PhotonNetwork.playerList.Length == 1)
            playerNum = 1;
    }

    void OnGUI()
    {
        if (!PhotonNetwork.connected)
        {
            GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
        }
        else if (PhotonNetwork.room == null)
        {
            //create Room
			if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
            {
               

                roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
                roomOptions.CustomRoomProperties.Add("Player1Score", 0);
                roomOptions.CustomRoomProperties.Add("Player2Score", 0);

                roomOptions.IsVisible = true;
                roomOptions.IsOpen = true;
                roomOptions.MaxPlayers = 2;
                PhotonNetwork.CreateRoom(room_name, roomOptions, lobbyName);
              
            }
            //Join Room
            if (room_list != null)
            {
                for (int i = 0; i < room_list.Length; i++)
                {
                    if (GUI.Button(new Rect(100, 250 + (100 * i), 250, 100), "Join Room" + " " + room_list[i].name))
                    {
						PhotonNetwork.JoinRoom(room_list[i].name);
             
                    }
                }
            }
        }

    

    }

    void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(lobbyName);
    }

    void OnReceivedRoomListUpdate()
    {
        Debug.Log("Room was created");
        room_list = PhotonNetwork.GetRoomList();
    }

	void OnJoinedLobby()
    {
    
        Debug.Log("Connected to Lobby");
    }

    void OnJoinedRoom()
    {
        heroSelect.enabled = false;

        if (PhotonNetwork.playerList.Length == 1)
            playerNum = 1;
        if (PhotonNetwork.playerList.Length == 2)
            playerNum = 2;

        PostProcessing.Single.StartEntry();

        SpawnPlayer ();
    }


	public void RespawnPlayer()
	{

        if (playerNum == 1)
        {
            localPlayer.transform.position = new Vector3(-31, 0.2f, 32);
        }

        else if (playerNum == 2)
        {
            localPlayer.transform.position = new Vector3(31, 0.2f, -31);
        }

        PostProcessing.Single.StartEntry();

        localPlayer.GetComponent<PlayerStatus>().photonView.RPC("Reset", PhotonTargets.AllBufferedViaServer);

        PurgeAllEffects();

    }



    void SpawnPlayer()
	{
        Vector3 spawn_position = Vector3.zero; 

        if (playerNum == 1)
            spawn_position = new Vector3(-31, 0.2f, 32);

		else if(playerNum == 2)
			spawn_position =  new Vector3(31, 0.2f, -31);

		player = PhotonNetwork.Instantiate(HeroPrefabs[n].name, spawn_position, Quaternion.identity, 0) as GameObject;

		localFoV = Instantiate (FoV);

		localFoV.transform.position = player.transform.position;


        if (player.GetPhotonView().isMine)
        {
            localPlayer = player;
            localUI = Instantiate(UI, Vector3.zero, Quaternion.identity) as GameObject;
            localUI.GetComponent<PlayerUI>().mPlayerStatus = localPlayer.GetComponent<PlayerStatus>();
        }

		localFoV.transform.SetParent (localPlayer.transform);

    }

    public void HeroSelect(int _n)
    {
        n = _n;
    }

    public int getPlayerNo { get { return playerNum; } }

    void PurgeAllEffects()
    {
        GameObject [] effects = GameObject.FindGameObjectsWithTag("effect");

        for (int i = 0; i < effects.Length; i++)
        {
            Destroy(effects[i]);
        }

        PostProcessing.Single._matCC.SetFloat("_MaxAngle", 0);
    }
}