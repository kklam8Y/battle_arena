using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAnimatorControl : Photon.MonoBehaviour {

    public Animator player_anim;

    RaycastHit hit;

    PlayerStatus PS;
	PlayerControl PC;

    // Use this for initialization
    void Start () {
        player_anim.GetComponent<Animator>();
		PS = GetComponent<PlayerStatus>();
		PC = GetComponent<PlayerControl> ();


    }

    // Update is called once per frame
    void Update()
    {

//		AnimatorStateInfo animInfo = player_anim.GetCurrentAnimatorStateInfo (0);
//		if (animInfo.tagHash == Animator.StringToHash ("Base Layer.movement"))
//			player_anim.speed = 0.5f;

		GetCurrentAction ();
    }

    void GetCurrentAction()
	{
		AnimeSetIncapacitated (false);

		if (PS.mState.GetCurrentState == State.MOVING) 
		{
			AnimSetMoving ();
		}  
		else if (PS.mState.GetCurrentState == State.IDLE) //|| PS.mState.GetCurrentState == State.STUNNED) 
		{
			AnimSetIdle ();
		}  

		else if (PS.mState.GetCurrentState == State.STUNNED)
		{
			AnimeSetIncapacitated (true);
			EndAbility ();
			AnimSetIdle ();
		}

		else if (PS.mState.GetCurrentState == State.ROOTED)
		{
			AnimSetIdle ();
		}

		else if(PS.mState.GetCurrentState == State.DEAD) 
		{
			AnimSetDead ();
			EndAbility ();
		}

		if (PC.mAbilityUsed.GetCurrent == Ability.LMB) 
		{
			AnimSetUseAbilityLMB ();
		} 
		else if (PC.mAbilityUsed.GetCurrent == Ability.RMB) {
			AnimSetUseAbilityRMB ();
		} 
		else if (PC.mAbilityUsed.GetCurrent == Ability.Q) {
			AnimSetUseAbilityQ ();
		} 
		else if (PC.mAbilityUsed.GetCurrent == Ability.SPACE) 
		{
			AnimSetUseAbilitySPACE ();
		}

		CAbility ability = PS.myHero.GetAbility (PC.mAbilityUsed.GetCurrent);

		if (!ability.getProperty.canMove) {
			player_anim.SetBool ("Moving", false);
		}

		if (PC.mAbilityUsed.GetCurrent != Ability.NONE)
		{
			
		}
			
    }

	void StartAbility()
	{
		//player_anim.SetBool ("inProgress", true);
	}

	void StartCD()
	{
		PS.ResetAbilityCD (PC.mAbilityUsed.GetCurrent);

		if (!PS.myHero.GetAbility(PC.mAbilityUsed.GetCurrent).getProperty.canMove)
			PC.rotation_smoothness = 0;
		else 
			PC.rotation_smoothness = 0.5f;
	}

	void ExAbility()
	{
		if (PC.photonView.isMine)
		{
			if(PS.mState.GetCurrentState != State.STUNNED)
			{
                Vector3 pos = Vector3.zero;

                Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (photonView.isMine)
                {
                    if (Physics.Raycast(r, out hit, 100, 5 << 6))
                    {
                        if (hit.collider.tag == "ground")
                        {

                            pos = hit.point;

                        }
                    }
                }

                PC.photonView.RPC ("CreateAbilityByType", PhotonTargets.All, PC.mAbilityUsed.GetCurrent, pos);
				PS.photonView.RPC ("ResourceCost", PhotonTargets.All, PS.myHero.GetAbility (PC.mAbilityUsed.GetCurrent).getProperty.getPResource.Cost);
			}
		}
	}

	public void EndAbility()
    {
		player_anim.SetBool ("UsingAbilityLMB", false);
		player_anim.SetBool ("UsingAbilityRMB", false);
		player_anim.SetBool ("UsingAbilityQ", false);
		player_anim.SetBool ("UsingAbilitySPACE", false);
		PC.mAbilityUsed.SetAbility (Ability.NONE);
		PC.sync_ability_used = Ability.NONE;
		//player_anim.SetBool ("inProgress", false);
		PC.rotation_smoothness = 20;
    }

	void AnimSetIdle()
	{
		player_anim.SetBool("Moving", false);
        player_anim.SetBool("Dead", false);
	}
	void AnimSetMoving()
	{
		player_anim.SetBool("Moving", true);
	}

	void AnimSetUseAbilityLMB()
	{
		player_anim.SetBool("UsingAbilityLMB", true);
	}

	void AnimSetUseAbilityRMB()
	{
		player_anim.SetBool("UsingAbilityRMB", true);
	}

	void AnimSetUseAbilityQ()
	{
		player_anim.SetBool("UsingAbilityQ", true);
	}

	void AnimSetUseAbilitySPACE()
	{
		player_anim.SetBool("UsingAbilitySPACE", true);
	}
		
	void AnimSetDead()
	{
		player_anim.SetBool ("Dead", true);
	}

	void AnimeSetIncapacitated(bool _incap)
	{
		player_anim.SetBool ("Incapacitated", _incap);
	}
}
