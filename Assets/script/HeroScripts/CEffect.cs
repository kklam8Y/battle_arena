﻿using System;
using UnityEngine;

public enum Effect
{
	NONE,
	OVER_TIME,
	DELAYED

};

public enum ECCType
{
	STUN,
	ROOT,
	SLOW
};

public abstract class CEffect
{
	public CEffect ()
	{;}

    public bool isAOE = false;

	abstract public void Execute (ref PlayerStatus playerStatus);
	abstract public State ApplyState(ref PlayerStatus state);
	abstract public void Remove(ref PlayerStatus state, bool _isOwner);

	public virtual SResource getTimer ()
	{
		return new SResource(0);
	}

	public virtual void setNextDamageTime(float time)
	{;}

	public virtual float getSlowAmount(){return 0;}
}

public class CEffectOverTime : CEffect
{
	SResource DoT_timer;
	float interval;
	int damage;
	int healing;
	float nextDamageTime;

	public CEffectOverTime (float _interval, float _duration, int _damage, int _healing)
	{
		damage = _damage;
		healing = _healing;

		DoT_timer = new SResource (_duration);
		interval = _interval;
	}
		
	public override SResource getTimer(){ return DoT_timer; }
	public override void setNextDamageTime(float time)
	{
		nextDamageTime = time;
	}

	public override void Execute(ref PlayerStatus playerStatus)
	{
		if (Time.time > nextDamageTime) 
		{
			playerStatus.photonView.RPC ("TakeDamage", PhotonTargets.AllBufferedViaServer, damage);
			playerStatus.photonView.RPC ("TakeHealing", PhotonTargets.AllBufferedViaServer, healing);
			nextDamageTime = Time.time + interval;
		}
	}

	public override State ApplyState(ref PlayerStatus state)
	{
		return state.mState.GetCurrentState;
	}

	public override void Remove (ref PlayerStatus state, bool _isOwner)
	{
        if (_isOwner)
        {
            state.photonView.RPC("TakeDamage", PhotonTargets.AllBufferedViaServer, damage);
            state.photonView.RPC("TakeHealing", PhotonTargets.AllBufferedViaServer, healing);
        }
    }

}

public class CEffectDelay : CEffect
{
	float delay; 
	int damage, healing;
	SResource DelayTimer;
	float nextDamageTime;

	public CEffectDelay (float _delay, int _damage, int _healing)
	{
		delay = _delay;
		damage = _damage;
		healing = _healing;
		DelayTimer = new SResource (_delay);

	}

	public override SResource getTimer(){ return DelayTimer; }

	public override void setNextDamageTime(float time)
	{
		nextDamageTime = time + delay;
	}

	public override void Execute(ref PlayerStatus playerStatus)
	{
		if (Time.time > nextDamageTime) 
		{
			playerStatus.photonView.RPC ("TakeDamage", PhotonTargets.AllBufferedViaServer, damage);
			playerStatus.photonView.RPC ("TakeHealing", PhotonTargets.AllBufferedViaServer, healing);

			nextDamageTime = 0;
		}
	}

	public override State ApplyState(ref PlayerStatus state)
	{
		return state.mState.GetCurrentState;
	}

	public override void Remove (ref PlayerStatus state, bool _isOwner)
    { 
		;
	}


}

public class CEffectCC : CEffect
{
	ECCType mCCType;
	SResource stun_timer;
	float amountPercentSlow;

	public CEffectCC (ECCType _Effect, float _duration)
	{
		mCCType = _Effect;
		stun_timer = new SResource (_duration);

	}

	public CEffectCC (float _duration, float _amountPercentSlow)
	{
		mCCType = ECCType.SLOW;
		amountPercentSlow = _amountPercentSlow;

		stun_timer = new SResource (_duration);

	}

	public override SResource getTimer(){ return stun_timer; }
	public override float getSlowAmount() { return amountPercentSlow; }

	public override State ApplyState (ref PlayerStatus state)
	{
		if (mCCType == ECCType.STUN)
			return State.STUNNED;
		if (mCCType == ECCType.ROOT)
			return State.ROOTED;
		if (mCCType == ECCType.SLOW)
			return State.SLOWED;

		return State.IDLE;
	}

	public override void Execute (ref PlayerStatus playerStatus)
	{
		;
	}

	public override void Remove (ref PlayerStatus state, bool _isOwner)
	{
		if (mCCType != ECCType.SLOW)
			state.mState.SetState (State.IDLE);
		else
			state.myHero.MoveSpeed.current = state.myHero.MoveSpeed.GetOriginal();
	}
}
