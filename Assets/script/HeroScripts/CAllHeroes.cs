﻿using UnityEngine;

public class CHeroZugZug : CHero
{
	private const string name = "ZugZug";
	private const int max_health = 500;
	private const double base_move_speed = 100.0d;

	public CHeroZugZug () : base (name, max_health, base_move_speed)
	{
		Health.current = max_health;
		Resource.current = 25;
		//LMB
		CResourceProperties mResPropLMB = new CResourceProperties(0, 10);
		CProperties mPropertyLMB = new CProperties(mResPropLMB, new Vector3(0, 2, 2.25f));
		base.Abilities [0] = new CAbility ("OrcAttack", 45, 0, 1.2f, mPropertyLMB);

		//RMB 
		CResourceProperties mResPropRMB = new CResourceProperties(50, 0);
		CProperties mPropertyAbilityRMB = new CProperties(mResPropRMB, new Vector3(0, 2, 0), false, false);
		base.Abilities [1] = new CAbility ("Shout", 2, 100, 10.0f, mPropertyAbilityRMB);

		//Q
		CProperties mPropertyAbilityQ = new CProperties(new Vector3(0, 1, 2), false);
		CEffectCC mEffectQ = new CEffectCC (ECCType.STUN, 2);
		base.Abilities [2] = new CAbilityProjectile ("BoulderToss", 14.0f, 30.0f, 0, 50, 6.0f, mPropertyAbilityQ, mEffectQ);
       
		//SPACE
		CProperties mPropertyAbiliySPACE = new CProperties(new Vector3(0,0.1f,1.0f), false, false);
		base.Abilities [3] = new CAbilityDash ("Charge", 20.0f, 50.0f, 150, 0, 4.0f, mPropertyAbiliySPACE);
	}
}

public class CHeroJieTu : CHero
{
	private const string name = "JieTu";
	private const int max_health = 300;
	private const double base_move_speed = 110.0d;


	public CHeroJieTu () : base (name, max_health, base_move_speed)
	{
		Health.current = max_health;
        Resource.current = 25;

        //LMB
        CResourceProperties mResPropLMB = new CResourceProperties(0, 10);
		CProperties mPropertyLMB = new CProperties(mResPropLMB, new Vector3(0, 1.7f, 1.2f), true, false);
		base.Abilities [0] = new CAbilityProjectile ("SoulArrow", 10.5f, 20, 15, 0, 1.0f, mPropertyLMB);

		//RMB
		CResourceProperties mResPropRMB = new CResourceProperties(25, 0);
		CProperties mPropertyRMB = new CProperties(mResPropRMB, new Vector3(0, 1.7f, 1.2f), false, true);
		CEffectOverTime mEffectRMB = new CEffectOverTime (3, 9, 35, 0);
		base.Abilities [1] = new CAbilityProjectile ("ElectricalShpere", 60f, 10.0f, 20, 0, 4.0f,mPropertyRMB, mEffectRMB);

		//Q
		CProperties mPropertyQ = new CProperties(new Vector3(0, 1f, 0), true);
        CEffectCC mEffectQ = new CEffectCC(2, 60);
        SResource QTimer = new SResource(3);
        base.Abilities [2] = new CAbilityAOE ("Freezing Orb", 30, 0, 10, QTimer, 1, false, mEffectQ);
        //base.Abilities [2].mEffect = new CEffectDelay (3.4f);

        //SPACE
        base.Abilities [3] = new CAbilityTeleport ("Teleport", 0, 0, 10, 5, 12);

	}


}

public class CHeroSkeleton : CHero
{
    private const string name = "Clank";
    private const int max_health = 600;
    private const double base_move_speed = 90.0d;

    public CHeroSkeleton() : base(name, max_health, base_move_speed)
    {
        Health.current = max_health;
        Resource.current = 100;
        //LMB
        CResourceProperties mResPropLMB = new CResourceProperties(0, 10);
        CProperties mPropertyLMB = new CProperties(mResPropLMB, new Vector3(0, 2, 2.25f));
        base.Abilities[0] = new CAbility("SkeletonAttack", 35, 0, 0.0f, mPropertyLMB);

        //RMB 
        CResourceProperties mResPropRMB = new CResourceProperties(20, 0);
        CProperties mPropertyAbilityRMB = new CProperties(mResPropRMB, new Vector3(0, 2, 2.25f), false, false);
        CEffectCC mEffectRMB = new CEffectCC(ECCType.ROOT, 2);
        base.Abilities[1] = new CAbility("SkeletonGrip", 150, 20, 6.0f, mPropertyAbilityRMB, mEffectRMB);

        //Q
        CResourceProperties mResPropQ = new CResourceProperties(0, 50);
        CProperties mPropertyAbilityQ = new CProperties(mResPropQ, new Vector3(0, 1, 2), false);
        base.Abilities[2] = new CAbility("SkeletonSlash", 200, 0, 0, mPropertyAbilityQ);

        //SPACE
        CProperties mPropertyAbiliySPACE = new CProperties(new Vector3(0, 0.1f, 1.0f), false, false);
        SResource SPACETimer = new SResource(2.5f);
        base.Abilities[3] = new CAbilityAOE("FlameBreath", 20, 0, 0, SPACETimer, 0.5f, true, mPropertyAbiliySPACE);
    }
}

//public class CDummyHero : CHero
//{
//	private const string name = "Dummy";
//	private const int max_health = 10;
//	private const double base_move_speed = 0.0d;
//
//
////	public CDummyHero () : base (name, max_health, base_move_speed)
////	{
////		Health.current = max_health;
////
////		base.Abilities [0] = new CAbilityProjectile ("ShadowBolt", 7.5f, 20, 10, 0, 0, true, false);
//////		base.Abilities [0].offset = new Vector3 (0, 1, 1.2f);
////		//base.Abilities[0].AddEffect (Effect.STUN, 2.0f);
////
////		base.Abilities [1] = new CAbilityProjectile ("ElectricalShpere", 15f, 10.0f, 20, 0, 4.0f, false, true);
////	//	base.Abilities [1].offset = new Vector3 (0, 1, 1.2f);
////		//base.Abilities [1].Effect = stun;
////		base.Abilities [2] = new CAbility ("WitchExplosion", 100, 0, 0, new Vector3(0,1.5f, 0) , true);
////		//base.Abilities [2].mEffect = new CEffectDelay (3.4f);
////
////		base.Abilities [3] = new CAbilityTeleport ("Teleport", 0, 0, 10, 5, 12);
////
////	}
//
//
//}
