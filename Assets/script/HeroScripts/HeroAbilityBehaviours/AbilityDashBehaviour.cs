﻿using UnityEngine;
using System.Collections;

public class AbilityDashBehaviour : MonoBehaviour 
{

	public GameObject instigator;
	public Ability mAbilityKey;

	public LayerMask layerMaskEnv;

	CHero mHero;
	CAbilityDash mAbility;
	CEffect mAbilityEffect;

	private Vector3 initial_pos;
	private Vector3 destination_pos;

	[SerializeField] Collider col_i;
	bool collided = false;

	[SerializeField] Collider col_ground;

	[SerializeField] private PlayerAnimatorControl mAnimator;
	[SerializeField] private EffectSpawner mAbilityEffectsController;

	float start_time;
	float distCovered;

	public ParticleSystem mParticles;

	bool mCollider = false;

	Rigidbody rig;

	RaycastHit hit;

	void Awake()
	{
		mHero = instigator.GetComponent<PlayerStatus> ().myHero;
		mAbility = mHero.GetAbility (mAbilityKey) as CAbilityDash;
		mAbilityEffect = mAbility.getEffect;

		mAnimator = instigator.GetComponent<PlayerAnimatorControl> ();
		col_i = instigator.GetComponent<Collider> ();

		mAbilityEffectsController = GetComponent<EffectSpawner> ();

		col_ground = GameObject.FindGameObjectWithTag ("ground").GetComponent<Collider> () as Collider;
	}

	// Use this for initialization
	void Start () {
		transform.rotation = instigator.transform.rotation;

		Renderer inst_renderer = instigator.GetComponentInChildren<Renderer> ();

		initial_pos = instigator.transform.position + (transform.forward * mAbility.getProperty.Offset.z) + (transform.up * mAbility.getProperty.Offset.y);
		//initial_pos.z += inst_renderer.bounds.size.z + 2;

		//transform.Translate(0, inst_renderer.bounds.size.y / 2, inst_renderer.bounds.size.z + 2);

		//Debug.Log (mAbilityEffects[0].getEffect);

		transform.position = initial_pos;

		start_time = Time.time;
		Vector3 rot = instigator.transform.rotation.eulerAngles;

		destination_pos = mAbility.GetDestinationPos (initial_pos, rot.y);

		//Debug.Log (initial_pos);
		//Debug.Log (destination_pos);
		//Debug.Log (mAbilityEffect.GetDuration());
		Physics.IgnoreCollision (col_ground, col_i);
		transform.SetParent (instigator.transform);

		rig = GetComponent<Rigidbody> ();
		//
	}

	// Update is called once per frame
	void Update () 
	{
		//Physics.IgnoreCollision (col_ground, col_i);
		distCovered = (Time.time - start_time) * mAbility.getSpeed;
		float progress = distCovered / mAbility.Distance;

		instigator.transform.position = Vector3.Lerp (initial_pos, destination_pos, progress);

		if (instigator.transform.position == destination_pos || collided) {
			DestroyObject ();
			instigator.GetComponent<PlayerControl> ().mAbilityUsed.SetAbility (Ability.NONE);
		}

	


	}

	void OnTriggerEnter(Collider c)
	{
		//Vector3 dir = new Vector3 (0,0,0);
		//dir.x = transform.position.x + Mathf.Sin (instigator.transform.rotation.y * Mathf.Deg2Rad) * 1;
		//dir.z = transform.position.z + Mathf.Cos (instigator.transform.rotation.y * Mathf.Deg2Rad) * 1;

		//if (Physics.Raycast(transform.position, dir, GetComponent<Collider>().bounds.z))//transform.position, (c.transform.position - transform.position).normalized, out hit,10, 7<<8))
	
		if (mAbility.getProperty.canPierce)
			Physics.IgnoreCollision (c, col_i);
		else 
		{
            if (c != col_ground)
            {
                collided = true;
                Instantiate(mParticles, transform.position + transform.forward * ((GetComponentInParent<Collider>().bounds.size.z / 2) - 1) + Vector3.up * GetComponentInParent<Collider>().bounds.size.y, Quaternion.identity);
            }

                //destination_pos = transform.position;
         }


		if (c.gameObject.tag == "Player" && c.gameObject != instigator.gameObject) 
		{
			PlayerStatus CPS = c.GetComponent<PlayerStatus> ();

            collided = true;

            if (CPS.photonView.isMine)
			{
				CPS.photonView.RPC ("TakeDamage", PhotonTargets.AllBufferedViaServer, mAbility.getDamage);
                DestroyObject();
            }
			if (mAbilityEffectsController != null)
			{
				mAbilityEffectsController.CreateEffect (c.transform, CPS, mAbility);
			}
		} 
			
	}

	void OnTriggerExit(Collider c)
	{
		//if(collided)
			Physics.IgnoreCollision (c, col_i, false);
	}

	void DestroyObject()
	{
		Physics.IgnoreCollision(col_ground, col_i, false);
		mAnimator.EndAbility ();
		transform.parent = null;
        instigator.GetComponent<Rigidbody>().velocity = Vector3.zero;
		Destroy (gameObject);
	}
}
