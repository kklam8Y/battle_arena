﻿using UnityEngine;
using System.Collections;

public class AbilityTeleportBehaviour : MonoBehaviour {

	public GameObject instigator;
	public Ability mAbilityKey;
	public Vector3 desiredPos;
	public float distance;

	CHero mHero;
	CAbilityTeleport mAbility;

	// Use this for initialization
	void Start () {
		mHero = instigator.GetComponent<PlayerStatus> ().myHero;
		mAbility = mHero.GetAbility (mAbilityKey) as CAbilityTeleport;

		distance = Vector3.Distance (instigator.transform.position, desiredPos);

            if (distance < mAbility.GetMaxRange && distance > mAbility.GetMinRange)
                instigator.transform.position = desiredPos;
            else
                instigator.transform.position = mAbility.GetDestinationPos(instigator.transform.position, instigator.transform.rotation.eulerAngles.y);
		//Debug.Log (desiredPos);
	}

	void Update()
	{
		if(instigator.GetComponent<PlayerControl>().mAbilityUsed.GetCurrent == Ability.NONE)
			Destroy(gameObject);
	}
}
