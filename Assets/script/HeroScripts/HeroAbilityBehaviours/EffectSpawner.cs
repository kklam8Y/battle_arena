﻿using UnityEngine;
using System.Collections;

public class EffectSpawner : MonoBehaviour
{

	public GameObject EffectPrefab;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateEffect(Transform target_transform, PlayerStatus _CPS, CAbility _mAbility)
	{
		GameObject newEffect;

		newEffect = Instantiate (EffectPrefab, target_transform.transform.position + Vector3.up * 4, Quaternion.identity) as GameObject;

      //  if(!_mAbility.getEffect.isAOE)
		newEffect.transform.SetParent (target_transform.transform);

		newEffect.GetComponent<EffectBehaviour> ().target_PS = _CPS;
		newEffect.GetComponent<EffectBehaviour> ().mAbility = _mAbility;

        newEffect.tag = "effect";
	}

}
