﻿using UnityEngine;
using System.Collections.Generic;

public class AbilityProjectileBehaviour : MonoBehaviour
{

	public GameObject instigator;
	public Ability mAbilityKey;

	public LayerMask layerMaskEnv;

	CHero mHero;
	CAbilityProjectile mAbility;
	CEffect mAbilityEffect;

	private Vector3 initial_pos;
	private Vector3 destination_pos;


	bool collided = false;

	[SerializeField] private PlayerAnimatorControl mAnimator;
	[SerializeField] private EffectSpawner mAbilityEffectsController;

	float start_time;
	float distCovered;

	public ParticleSystem mParticles;

	bool mCollider = false;

	Collider col_i, col_t;
	Collider col_ground;

	Rigidbody rig;

	void Awake()
	{
		mHero = instigator.GetComponent<PlayerStatus> ().myHero;
		mAbility = mHero.GetAbility (mAbilityKey) as CAbilityProjectile;
		mAbilityEffect = mAbility.getEffect;

		mAnimator = instigator.GetComponent<PlayerAnimatorControl> ();
		col_i = instigator.GetComponent<Collider> ();
		col_t = GetComponent<Collider> ();

		mAbilityEffectsController = GetComponent<EffectSpawner> ();

		col_ground = GameObject.FindGameObjectWithTag ("ground").GetComponent<Collider> () as Collider;

		Physics.IgnoreCollision (col_ground, col_t);
		Physics.IgnoreCollision (col_i, col_t);
	}

	// Use this for initialization
	void Start () 
	{

		transform.rotation = instigator.transform.rotation;

		Renderer inst_renderer = instigator.GetComponentInChildren<Renderer> ();

		initial_pos = instigator.transform.position + (transform.forward * mAbility.getProperty.Offset.z) + (transform.up * mAbility.getProperty.Offset.y);
		//initial_pos.z += inst_renderer.bounds.size.z + 2;

		//transform.Translate(0, inst_renderer.bounds.size.y / 2, inst_renderer.bounds.size.z + 2);

		//Debug.Log (mAbilityEffects[0].getEffect);

		transform.position = initial_pos;

		start_time = Time.time;
		Vector3 rot = instigator.transform.rotation.eulerAngles;

		destination_pos = mAbility.GetDestinationPos (initial_pos, rot.y);

		//Debug.Log (initial_pos);
		//Debug.Log (destination_pos);
		//Debug.Log (mAbilityEffect.GetDuration());


		rig = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update ()
	{

		//destination_pos.y = GetYPos(transform.position.x);

		//transform.LookAt (destination_pos);
		distCovered = (Time.time - start_time) * mAbility.getSpeed;
		float progress = distCovered / mAbility.Distance;

	
	
		transform.position = Vector3.Lerp (initial_pos, destination_pos, progress);


		if (transform.position == destination_pos) 
		{
			Destroy (gameObject);
		}
			
	}

	void OnTriggerEnter (Collider c)
	{
		

		if (mAbility.getProperty.canPierce || c == col_i)
			Physics.IgnoreCollision (c, col_t);
		else 
		{
			Destroy (gameObject);
		}

		if (c.tag != "ground") 
		{
			if (mParticles == null)
				Debug.Log ("<color=green>particle it missing! </color>");
			else if (mParticles != null && c.gameObject != instigator.gameObject)
			{
				Instantiate (mParticles, gameObject.transform.position, Quaternion.identity);
				mCollider = true;
			}
		} 

		//if (mAbility.getProperty.canPierce)
		//	Physics.IgnoreCollision (c, col_i);
		//else 
		//{
		//	Destroy (gameObject);
		//}

		if (c.gameObject.tag == "Player" && c.gameObject != instigator.gameObject) 
		{
			PlayerStatus CPS = c.GetComponentInParent<PlayerStatus> ();
			PlayerStatus PS = instigator.GetComponent<PlayerStatus> ();


			if (CPS.photonView.isMine) 
			{
				CPS.photonView.RPC ("TakeDamage", PhotonTargets.AllBufferedViaServer, mAbility.getDamage);
				PS.photonView.RPC ("ResourceGain", PhotonTargets.AllBufferedViaServer, mAbility.getProperty.getPResource.Gain);
			}
			if (mAbilityEffectsController != null) 
			{
				mAbilityEffectsController.CreateEffect (c.transform, CPS, mAbility);
			}
			if (!mAbility.getProperty.canPierce)
				Destroy (gameObject);
		}
	}

	//void OnTriggerEnter (Collider c)
	//{
	//	;
	//}

}



	//source http://stackoverflow.com/questions/35183365/unity-destination-vector-based-on-rotation-and-the-amount-to-move 11/11/2016

//	float GetYPos(float _x)
//	{
//		float y0 = 3;
//		float x = _x;
//		float g = Physics.gravity.magnitude;
//		float theta = ConvertToRadians(45);
//		float v = mSpeed;
//
//		float vcosTheta =  (Mathf.Cos (theta) * 2) * (v *2);
//		vcosTheta *= Mathf.Pow(vcosTheta,2);
//
//		float themp = g * Mathf.Pow (x, 2) / vcosTheta;
//
//		return y0 + x * Mathf.Tan (theta) - themp;
//
//	}
//}
