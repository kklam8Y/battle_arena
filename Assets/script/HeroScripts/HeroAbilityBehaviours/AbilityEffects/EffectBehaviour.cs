using UnityEngine;
using System.Collections;

public class EffectBehaviour : MonoBehaviour
{
    public CAbility mAbility;
    public PlayerStatus target_PS;
    CEffect mEffect;
    SResource timer;
    public Material _mat;

    // Use this for initialization
    void Start()
    {
        mEffect = mAbility.getEffect;

        timer = mEffect.getTimer();

        mEffect.setNextDamageTime(Time.time);

        float height = transform.parent.GetComponent<Collider>().bounds.size.y;

        transform.position = new Vector3(transform.position.x, height , transform.position.z);

        //if(target_PS.mState.GetCurrentState != State.SLOWED)
            target_PS.myHero.MoveSpeed.current = target_PS.myHero.MoveSpeed.current - target_PS.myHero.MoveSpeed.current * (mEffect.getSlowAmount() / 100);

    }

    // Update is called once per frame
    void Update()
    {


        timer.current += Time.deltaTime;
        //target_PS.durationDisable = timer;

        target_PS.mState.SetState(mEffect.ApplyState(ref target_PS));

        if (target_PS.photonView.isMine)
        {
            mEffect.Execute(ref target_PS);
            _mat.SetFloat("_MaxAngle", ((timer.GetMax() - timer.current) / timer.GetMax() * 360));
        }

        if (timer.current >= timer.GetMax())
        {
            mEffect.Remove(ref target_PS, target_PS.photonView.isMine);
            Destroy(gameObject);
        }
    }

}
