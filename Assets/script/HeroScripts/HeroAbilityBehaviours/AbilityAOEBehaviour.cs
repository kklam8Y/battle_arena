﻿using UnityEngine;
using System.Collections;

public class AbilityAOEBehaviour : MonoBehaviour {

    public GameObject instigator;
    public Ability mAbilityKey;
    public Vector3 desiredPos;

    CHero mHero;
    CAbilityAOE mAbility;

    PlayerStatus CPS;
    PlayerStatus PS;

    Collider mCollider;

    EffectSpawner mAbilityEffectsController;

    public ParticleSystem mParticlesHit;

    bool madeEffect;

    // Use this for initialization
    void Start() {
        mHero = instigator.GetComponent<PlayerStatus>().myHero;
        mAbility = mHero.GetAbility(mAbilityKey) as CAbilityAOE;

        mAbility.SetDamageTimeStart(Time.time);

        if (mAbility.isAttached)
            transform.SetParent(instigator.transform);
        else


        transform.position = transform.position + Vector3.up * 2;

        mCollider = GetComponent<Collider>();

        mAbilityEffectsController = GetComponent<EffectSpawner>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mAbility.Progress(Time.deltaTime) > 1)
        {
            Destroy(gameObject);
            mAbility.Reset();
        }


    }

    void OnTriggerStay(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            CPS = c.GetComponent<PlayerStatus>();

            if (CPS.photonView.isMine)
            {
                if (c.gameObject != instigator.gameObject && mAbility.CanExecute(Time.time))
                {
                    Debug.Log("hit player");
                    CPS.photonView.RPC("TakeDamage", PhotonTargets.AllBufferedViaServer, mAbility.getDamage);
                    // PS.photonView.RPC("ResourceGain", PhotonTargets.AllBufferedViaServer, mAbility.getProperty.getPResource.Gain);
                
                }
         

            }

                if (mAbilityEffectsController != null && !madeEffect && c.gameObject != instigator)
                {
                    mAbilityEffectsController.CreateEffect(c.transform, CPS, mAbility);
                    madeEffect = true;
                }
           

      

        }
    }

    [PunRPC]
    void Pos(Vector3 _pos)
    {
        desiredPos = _pos;
    }
}
