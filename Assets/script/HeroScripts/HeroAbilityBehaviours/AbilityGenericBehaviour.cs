﻿using UnityEngine;
using System.Collections;

public class AbilityGenericBehaviour : MonoBehaviour {

	public GameObject instigator;
	public Ability mAbilityKey;

	CHero mHero;
	CAbility mAbility;

	EffectSpawner mAbilityEffectsController;

	public ParticleSystem mParticlesHit, mParticle;

	float ex_time = 0;
	bool mCollider = false;

	Vector3 dirToTarget, myPos;

	PlayerStatus CPS;
	PlayerStatus PS ;
	// Use this for initialization
	void Start () 
	{
		mHero = instigator.GetComponent<PlayerStatus> ().myHero;
		mAbility = mHero.GetAbility (mAbilityKey) as CAbility;

		ex_time = Time.time + 0.1f;

		mAbilityEffectsController = GetComponent<EffectSpawner> ();


		gameObject.transform.rotation = instigator.transform.rotation;
		gameObject.transform.position = instigator.transform.position + (transform.forward * mAbility.getProperty.Offset.z) +  (transform.up * mAbility.getProperty.Offset.y);

		transform.SetParent (instigator.transform);

		if (mParticle != null)
		{
			ParticleSystem mPS;
			mPS = Instantiate (mParticle, gameObject.transform.position, Quaternion.identity) as ParticleSystem;
			mPS.transform.SetParent (instigator.transform);
			mPS.transform.position = gameObject.transform.position;
		}

		PS = instigator.GetComponent<PlayerStatus> ();

		if(PS.photonView.isMine)
			PS.photonView.RPC ("TakeHealing", PhotonTargets.AllBufferedViaServer, mAbility.getHealing);
		Debug.Log ("Healed for:" + mAbility.getHealing);

        
    }
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time >= ex_time + 0.1f)
			Destroy (gameObject);
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.tag != "ground") 
		{
			if (mParticlesHit == null)
				Debug.Log ("<color=red>particle it missing! </color>");
			else if (mParticlesHit != null && c.gameObject != instigator.gameObject && !mCollider) {
				Instantiate (mParticlesHit, gameObject.transform.position, Quaternion.identity);
				mCollider = true;
			}
		}

		if (c.gameObject.tag == "Player")
		{
			CPS = c.GetComponent<PlayerStatus> ();

			Debug.Log("Healed for:" + mAbility.getHealing);

            if (CPS.photonView.isMine)
            {


                if (c.gameObject != instigator.gameObject)
                {
                    CPS.photonView.RPC("TakeDamage", PhotonTargets.AllBufferedViaServer, mAbility.getDamage);
                    PS.photonView.RPC("ResourceGain", PhotonTargets.AllBufferedViaServer, mAbility.getProperty.getPResource.Gain);
                }

                if (mAbilityEffectsController != null)
                {
                    mAbilityEffectsController.CreateEffect(c.transform, CPS, mAbility);
                }


            }
      
        }

    }
}
