﻿using UnityEngine;

public class CHero
{
	public SResource Health;
	public SResource Resource;
	public SMoveSpeed MoveSpeed;

	protected CAbility[] Abilities;

	//Hero var:
	private string m_name;

	public double m_move_speed;

	//constructor
	public CHero(string _name, int _max_health, double _move_speed)
	{
		m_name = _name;

		Health = new SResource (_max_health);
		Resource = new SResource (100);
		MoveSpeed = new SMoveSpeed (_move_speed);
		MoveSpeed.current = _move_speed;

		Abilities = new CAbility[5];

	}

	public void TakeDamage(int damage)
	{
		Health.current -= damage;
	}

	public void TakeHealing(int healing)
	{
		Health.current += healing;

		Health.current = Mathf.Clamp (Health.current, 0, Health.GetMax ());
	}

	public CAbility GetAbility(Ability _ability)
	{
		CAbility ability = new CAbility();

		switch (_ability) 
		{
		case Ability.LMB:
			ability = Abilities [0];
			break;
		case Ability.RMB:
			ability = Abilities [1];
			break;
		case Ability.Q:
			ability = Abilities [2];
			break;
		case Ability.SPACE:
			ability = Abilities [3];
			break;		
		}

		return ability;
	}

	public CAbility GetAbility(int _ability_index)
	{
		return Abilities[_ability_index];
	}

	public int getAbilityLength{get{return Abilities.Length;}}

//	public bool isDead()
//	{
//		if (Health.current <= 0)
//			return true;
//		else
//			return false;
//	}
}

public struct SResource
{
	private float max;
	public float current;

	public SResource(float _max)
	{
		max = _max;
		current = 0;
	}

	public float GetMax()
	{
		return max;
	}

	public void Reset()
	{
		current = max;
	}

    public bool Peak()
    {
        if (current >= max)
            return true;

        return false;
    }

    public void CountDown(float _amount)
    {
        current -= _amount;
        current = Mathf.Clamp(current, 0, max);

    }

    public bool isReady()
    {
        if (current == 0)
            return true;

       return false;
    }
}

public struct SMoveSpeed
{
	private double original;
	public double current;
	
	public SMoveSpeed(double _original)
	{
		original = _original;
		current = 0;
	}
	
	public double GetOriginal()
	{
		return original;
	}
}

public enum State
{
	IDLE,
	MOVING,
	DEAD,
	USING_ABILITY,
	STUNNED,
	ROOTED,
	SLOWED
};

public struct SState
{
	public State State;

	public State GetCurrentState{get{ return State;}}
	public void SetState(State _state)
	{
		State = _state;
	}

}

public enum Ability
{
	NONE,
	LMB,
	RMB,
	Q,
	SPACE
};

public struct SAbility
{
	public Ability ability;

	public Ability GetCurrent{get{ return ability;}}
	public void SetAbility(Ability _ability)
	{
		ability = _ability;
	}

}

