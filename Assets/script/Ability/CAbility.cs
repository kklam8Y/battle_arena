﻿using System;
using UnityEngine;
using System.Collections.Generic;

public enum Attachment
{
    PLAYER,
    FREE
}

public class CAbility
{
	string name = "Default";
	int damage;
	int healing;
	protected AbilityType Type = AbilityType.GENERIC;

    public Vector2 clickPos;

	SResource Cooldown;
	CProperties Properties = new CProperties ();
	CEffect mEffect;

	public CAbility (string _name, int _damage, int _healing, float _cooldown)
	{
		damage = _damage;
		healing = _healing;

		Cooldown = new SResource (_cooldown);
		Cooldown.current = 0;
		name = _name;
	}

	public CAbility (string _name, int _damage, int _healing, float _cooldown, CProperties _Properties)
	{
		damage = _damage;
		healing = _healing;

		Cooldown = new SResource (_cooldown);
		Cooldown.current = 0;
		name = _name;

		Properties = _Properties;
	}

    public CAbility(string _name, int _damage, int _healing, float _cooldown, CEffect _effect)
    {
        damage = _damage;
        healing = _healing;

        Cooldown = new SResource(_cooldown);
        Cooldown.current = 0;
        name = _name;

        mEffect = _effect;
    }

    public CAbility (string _name, int _damage, int _healing, float _cooldown, CProperties _Properties, CEffect _Effect)
	{
		damage = _damage;
		healing = _healing;

		Cooldown = new SResource (_cooldown);
		Cooldown.current = 0;
		name = _name;

		Properties = _Properties;
		mEffect = _Effect;
	}

	public CAbility ()
	{
		damage = 0;
		healing = 0;
	}

	public virtual Vector3 GetDestinationPos (Vector3 _initial, float _angle)
	{
		return _initial;
	}


	public int getDamage{ get { return damage; } }

	public int getHealing{ get { return healing; } }

	public AbilityType getType{ get { return Type; } }

	public string getName{ get { return name; } }

	public CProperties getProperty{ get { return Properties; } }

	public CEffect getEffect{ get { return mEffect; } }

	public SResource getCD{ get { return Cooldown; } }
}


public enum AbilityType
{
	GENERIC,
	PROJECTILE,
	AOE,
	TELEPORT,
	DASH
};


public class CAbilityProjectile : CAbility
{
	private float distance;
	private float speed;

	public CAbilityProjectile ()
	{
		//start = new Vector3 (0, 0, 0);
		distance = 0.0f;
	}

	public CAbilityProjectile (string _name, float _distance, float _speed, int _damage, int _healing, float _cooldown, CProperties _Properties) : base (_name, _damage, _healing, _cooldown, _Properties)
	{
		base.Type = AbilityType.PROJECTILE;
		//start = new Vector3(0, 0, 0);
		distance = _distance;
		speed = _speed;
	}

	public CAbilityProjectile (string _name, float _distance, float _speed, int _damage, int _healing, float _cooldown, CProperties _Properties, CEffect _Effect) : base (_name, _damage, _healing, _cooldown, _Properties, _Effect)
	{
		base.Type = AbilityType.PROJECTILE;
		//start = new Vector3(0, 0, 0);
		distance = _distance;
		speed = _speed;
	}

	public CAbilityProjectile (string _name, float _distance, float _speed, int _damage, int _healing, float _cooldown, CProperties _Properties, AbilityType _ability_type) : base (_name, _damage, _healing, _cooldown)
	{
		base.Type = _ability_type;
		//start = new Vector3(0, 0, 0);
		distance = _distance;
		speed = _speed;
	}

	public float Distance{ get { return distance; } }

	public float getSpeed{ get { return speed; } }

	public override Vector3 GetDestinationPos (Vector3 _initial, float angle)
	{
		Vector3 destination;

		destination.x = _initial.x + Mathf.Sin (angle * Mathf.Deg2Rad) * distance;
		destination.y = _initial.y;
		destination.z = _initial.z + Mathf.Cos (angle * Mathf.Deg2Rad) * distance;

		return destination;
	}
	//	float ConvertToRadians(float angle)
	//	{
	//		return (Mathf.PI / 180f) * angle;
	//	}

}

public class CAbilityDash : CAbilityProjectile
{
	public CAbilityDash (string _name, float _distance, float _speed, int _damage, int _healing, float _cooldown, CProperties _Properties) : base (_name, _distance, _speed, _damage, _healing, _cooldown, _Properties)
	{
		base.Type = AbilityType.DASH;
	}
}

public class CAbilityTeleport : CAbility
{
	float max_range, min_range, distance;
	Vector3 dir;

	public CAbilityTeleport (string _name, int _damage, int _healing, float _cooldown, float _min_range, float _max_range) : base (_name, _damage, _healing, _cooldown)
	{
		base.Type = AbilityType.TELEPORT;
		min_range = _min_range;
		max_range = _max_range;
	}

	public override Vector3 GetDestinationPos (Vector3 _start_pos, float _angle)
	{
		Vector3 destination;

		//distance = Mathf.Clamp (distance, min_range, max_range);

		destination.x = _start_pos.x + Mathf.Sin (_angle * Mathf.Deg2Rad) * max_range;
		destination.y = _start_pos.y;
		destination.z = _start_pos.z + Mathf.Cos (_angle * Mathf.Deg2Rad) * max_range;

		return destination;
	}

	public float GetMaxRange{ get { return max_range; } }

	public float GetMinRange{ get { return min_range; } }
}


public class CAbilityAOE : CAbility
{
    bool attached = false;
    SResource Timer;
    float intervals;

    float nextDamageTime;
    float startTime;

    public CAbilityAOE(string _name, int _damage, int _healing, float _cooldown, SResource _Timer, float _intervals, bool _attached) : base(_name, _damage, _healing, _cooldown)
    {
        base.Type = AbilityType.AOE;
        Timer = _Timer;
        Timer.current = 0;
        intervals = _intervals;
        attached = _attached;
    }

    public CAbilityAOE(string _name, int _damage, int _healing, float _cooldown, SResource _Timer, float _intervals, bool _attached, CEffect _effect) : base(_name, _damage, _healing, _cooldown, _effect)
    {
        base.Type = AbilityType.AOE;
        Timer = _Timer;
        Timer.current = 0;
        intervals = _intervals;
        attached = _attached;
    }

    public CAbilityAOE(string _name, int _damage, int _healing, float _cooldown, SResource _Timer, float _intervals, bool _attached, CProperties _Properties) : base(_name, _damage, _healing, _cooldown, _Properties)
    {
        base.Type = AbilityType.AOE;
        Timer = _Timer;
        Timer.current = 0;
        intervals = _intervals;
        attached = _attached;
    }

    public void SetDamageTimeStart(float _nextDamageTime)
    {
        nextDamageTime = _nextDamageTime = startTime;
    }

    public float Progress(float time)
    {
        Timer.current += time;

        if (Timer.current + startTime > nextDamageTime)
            nextDamageTime += intervals;

        return Timer.current / Timer.GetMax();
    }

    public bool CanExecute(float time)
    {

        bool result = false;

        if (Time.time > nextDamageTime)
        {
            result = true;
            nextDamageTime = Time.time + intervals;
        }

        return result;
    }


    public bool isAttached { get { return attached; } }

    public void Reset()
    {
        Timer.current = 0;
    }
}

