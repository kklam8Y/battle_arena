﻿public class CResourceProperties
{
	int cost = 0;
	int gain = 0;

	public CResourceProperties(int _cost, int _gain)
	{
		cost = _cost;
		gain = _gain;
	}

	public int Cost{set { cost = value; } get{ return cost;}}
	public int Gain{set { gain = value; } get{ return gain;}}
}