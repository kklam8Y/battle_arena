﻿using UnityEngine;

public class CProperties
{
	Vector3 offset = new Vector3 (0, 1, 0);
	bool move = true;
	bool pierce = false;
	CResourceProperties mResourceProperties = new CResourceProperties(0,0);

	public CProperties ()
	{
		offset = new Vector3 (0, 1, 0);
		move = true;
		pierce = false;
		mResourceProperties = new CResourceProperties(0,0);
	}

	public CProperties (CResourceProperties _res_prop, Vector3 _offset)
	{
		mResourceProperties = _res_prop;
		offset = _offset;
	}

	public CProperties (bool _canMove)
	{
		canMove = _canMove;
	}

	public CProperties (Vector3 _offset, bool _canMove)
	{
		offset = _offset;
		canMove = _canMove;
	}

	public CProperties (CResourceProperties _res_prop, Vector3 _offset, bool _canMove, bool _canPierce)
	{
		mResourceProperties = _res_prop;
		offset = _offset;
		canMove = _canMove;
		pierce = _canPierce;
	}

	public CProperties (CResourceProperties _res_prop, Vector3 _offset, bool _canMove)
	{
		mResourceProperties = _res_prop;
		offset = _offset;
		canMove = _canMove;
	}

	public CProperties (Vector3 _offset, bool _canMove, bool _canPierce)
	{
		offset = _offset;
		canMove = _canMove;
		pierce = _canPierce;
	}

	public CProperties (bool _canMove, bool _canPierce)
	{
		canMove = _canMove;
		pierce = _canPierce;
	}

	public Vector3 Offset{ get { return offset; } }

	public bool canMove{ set { move = value; } get { return move; } }

	public bool canPierce{ set { pierce = value; } get { return pierce; } }

	public CResourceProperties getPResource{get {return mResourceProperties; } }


}
