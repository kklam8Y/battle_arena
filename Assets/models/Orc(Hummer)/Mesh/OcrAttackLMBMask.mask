%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: OcrAttackLMBMask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bone001
    m_Weight: 1
  - m_Path: Bone001/Hammer
    m_Weight: 1
  - m_Path: CATRigHub001
    m_Weight: 1
  - m_Path: CATRigHub001/Belt Detail
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone001
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone001/CATRigHub001Bone001Bone001
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone001/CATRigHub001Bone001Bone001/CATRigHub001Bone001Bone001Bone001
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone002
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone002/CATRigHub001Bone002Bone001
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone002/CATRigHub001Bone002Bone001/CATRigHub001Bone002Bone001Bone001
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigHub002Bone001
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigHub002Bone001/CATRigHub002Bone001Bone001
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigHub002Bone001/CATRigHub002Bone001Bone001/Teeth_up
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigHub002Bone001/CATRigHub002Bone001Bone002
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigHub002Bone001/CATRigHub002Bone001Bone003
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigHub002Bone001/Eyes
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigHub002Bone001/Teeth_down
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit11
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit11/CATRigLArmDigit12
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit11/CATRigLArmDigit12/CATRigLArmDigit13
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit21
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit21/CATRigLArmDigit22
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit21/CATRigLArmDigit22/CATRigLArmDigit23
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit31
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit31/CATRigLArmDigit32
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit31/CATRigLArmDigit32/CATRigLArmDigit33
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit41
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit41/CATRigLArmDigit42
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigLArmCollarbone/CATRigLArm1/CATRigLArm21/CATRigLArm22/CATRigLArmPalm/CATRigLArmDigit41/CATRigLArmDigit42/CATRigLArmDigit43
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit11
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit11/CATRigRArmDigit12
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit11/CATRigRArmDigit12/CATRigRArmDigit13
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit21
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit21/CATRigRArmDigit22
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit21/CATRigRArmDigit22/CATRigRArmDigit23
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit31
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit31/CATRigRArmDigit32
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit31/CATRigRArmDigit32/CATRigRArmDigit33
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit41
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit41/CATRigRArmDigit42
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigHub001Bone003/CATRigSpine1/CATRigSpine2/CATRigHub002/CATRigRArmCollarbone/CATRigRArm1/CATRigRArm21/CATRigRArm22/CATRigRArmPalm/CATRigRArmDigit41/CATRigRArmDigit42/CATRigRArmDigit43
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigLLeg1
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigLLeg1/CATRigLLeg2
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigLLeg1/CATRigLLeg2/CATRigLLegAnkle
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigLLeg1/CATRigLLeg2/CATRigLLegAnkle/CATRigLLegDigit11
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigRLeg1
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigRLeg1/CATRigRLeg2
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigRLeg1/CATRigRLeg2/CATRigRLegAnkle
    m_Weight: 1
  - m_Path: CATRigHub001/CATRigRLeg1/CATRigRLeg2/CATRigRLegAnkle/CATRigRLegDigit11
    m_Weight: 1
  - m_Path: CATRigLLegPlatform
    m_Weight: 1
  - m_Path: CATRigRLegPlatform
    m_Weight: 1
  - m_Path: Character01
    m_Weight: 1
  - m_Path: Character01/Body
    m_Weight: 1
  - m_Path: Character01/Cloth
    m_Weight: 1
  - m_Path: Character002
    m_Weight: 1
