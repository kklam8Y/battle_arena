﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

//ref 

Shader "Custom/FOWShader" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
		_FogRadius ("FogRadius", float) = 1.0
		_FogMaxRadius ("FogMaxRadius", float) = 0.5
		_PlayerPos ("PlayerPos", Vector) = (0, 0, 0, 1)
	}
	SubShader {
		Tags {"Queue" = "Transparent" "RenderType"="Transparent"}
		LOD 200
		Blend SrcAlpha OneMinusSrcAlpha
		//Cull off
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert alpha vertex:vert 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		float _FogRadius;
		float _FogMaxRadius;
		float4 _PlayerPos;

		struct Input {
			float2 uv_MainTex;
			float2 location;
		};

		float powerForPos(float4 pos, float2 nearVertex);

		void vert(inout appdata_full vertexData, out Input outData)
		{
			float4 pos = mul(UNITY_MATRIX_MVP, vertexData.vertex);
			float4 posWorld = mul(unity_ObjectToWorld, vertexData.vertex);
			outData.uv_MainTex = vertexData.texcoord;
			outData.location = posWorld.xz;
		}
	
		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 baseColor = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = baseColor.rgb;
			float alpha = (1.0 - powerForPos(_PlayerPos, IN.location));

			o.Alpha = alpha;
		}

		float powerForPos(float4 pos, float2 nearVertex)
		{
			float atten = (_FogRadius - length(pos.xz - nearVertex.xy));
			return (1.0/_FogMaxRadius) * atten/_FogRadius;
		}

		ENDCG
	}
	FallBack "Transparent/VertexLit"
}
